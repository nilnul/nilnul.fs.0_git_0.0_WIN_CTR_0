﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nilnul.fs._git_._WIN_CTR_.module_
{
	public partial class ByDiv : UserControl
	{
		public event Action report;
		public ByDiv()
		{
			InitializeComponent();
			_validate();
		}



		public nilnul.fs.git.ModuleI moduleNul => beOk ? nilnul.fs.git.Module.OvInnerDivAddress(address__shield__address__Folder1.val) : null;



		public string txt
		{
			get => this.address__shield__address__Folder1.val;
			set => this.address__shield__address__Folder1.val = value;
		}

		private bool _beOk;

		public bool beOk
		{
			get { return _beOk; }
			set { _beOk = value; }
		}

		bool _beOkByCompute()
		{
			return this.address__shield__address__Folder1.beOk && this.eg__herit__readonly__multiline__xpn__trigValid_WithInput11.xpnCtr.beOk;
		}

		public void disable()
		{
			this.address__shield__address__Folder1.Enabled = false;//.disable();
		}
		public void enable()
		{
			this.address__shield__address__Folder1.Enabled = false;//.enable();
		}

		void on()
		{
			var oldValid = _beOk;
			_validate();
			if (oldValid || beOk)
			{
				trig();
			}
		}

		void trig()
		{

			report?.Invoke();

		}
		private void _validate()
		{

			if (!this.address__shield__address__Folder1.beOk)
			{
				this.eg__herit__readonly__multiline__xpn__trigValid_WithInput11.xpn = "";
			}
			else
			{
				var module = nilnul.fs.address_.shield.sups._NearestGitModuleX.ShieldNulable_ofAddress(this.address__shield__address__Folder1.val);
				if (module == null)
				{
					this.eg__herit__readonly__multiline__xpn__trigValid_WithInput11.xpn = "not in module";
				}
				else
				{
					this.eg__herit__readonly__multiline__xpn__trigValid_WithInput11.xpn = "";
				}
			}

			_beOk = _beOkByCompute();
		}

		private void shieldAddressCtr_changedReportable()
		{
			on();

		}
	}
}
