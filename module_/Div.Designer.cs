﻿namespace nilnul.fs._git_._WIN_CTR_.module_
{
	partial class module__Div
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.shieldAddressCtr = new nilnul._fs_._WIN_CTR_.address_.shield_.address__shield__Address();
			this.xpn1 = new nilnul._txt_._WIN_CTR_.eg_.eg__Xpn();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.tableLayoutPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// shieldAddressCtr
			// 
			this.shieldAddressCtr.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.shieldAddressCtr.Location = new System.Drawing.Point(3, 3);
			this.shieldAddressCtr.Name = "shieldAddressCtr";
			this.shieldAddressCtr.Size = new System.Drawing.Size(497, 56);
			this.shieldAddressCtr.TabIndex = 0;
			this.shieldAddressCtr.txt = "";
			this.shieldAddressCtr.changedReportable += new System.Action(this.shieldAddressCtr_changedReportable);
			// 
			// xpn1
			// 
			this.xpn1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.xpn1.AutoScroll = true;
			this.xpn1.Location = new System.Drawing.Point(3, 65);
			this.xpn1.Name = "xpn1";
			this.xpn1.Size = new System.Drawing.Size(497, 37);
			this.xpn1.TabIndex = 1;
			this.xpn1.txt = "";
			this.xpn1.Visible = false;
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 1;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.Controls.Add(this.shieldAddressCtr, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.xpn1, 0, 1);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 2;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.Size = new System.Drawing.Size(503, 105);
			this.tableLayoutPanel1.TabIndex = 2;
			// 
			// module__Div
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.Controls.Add(this.tableLayoutPanel1);
			this.Name = "module__Div";
			this.Size = new System.Drawing.Size(503, 105);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		

		#endregion

		private nilnul._fs_._WIN_CTR_.address_.shield_.address__shield__Address shieldAddressCtr;
		private nilnul._txt_._WIN_CTR_.eg_.eg__Xpn xpn1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
	}
}
