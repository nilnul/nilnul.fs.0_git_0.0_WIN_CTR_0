﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace nilnul.fs._git_._WIN_CTR_.module_
{
	public partial class module__Div : UserControl
	{
		public event Action changedReportable;

		public module__Div()
		{
			InitializeComponent();
			validate();
		}

		public nilnul.fs.git.ModuleI module => validity? nilnul.fs.git.Module.OvTop(shieldAddressCtr.shieldNulable): null ;

		private bool _shieldValidity;

		public bool invalidity => !validity;
		public bool validity => _validity( shieldAddressCtr.beValid);

		public string txt
		{
			get => this.shieldAddressCtr.txt;
			set => this.shieldAddressCtr.txt= value;
		}

		bool _validity(bool shieldValidity) {
			return shieldValidity && xpn1.beValid;
		}

		public void disable() {
			this.shieldAddressCtr.disable();
		}
		public void enable() {
			this.shieldAddressCtr.enable();
		}

		void on() {
			var oldValid = _validity( _shieldValidity);
			validate();
			if (oldValid || validity)
			{
				trig();
			}
		}

		void trig() {

			changedReportable?.Invoke();

		}
		private void validate()
		{
			_shieldValidity = shieldAddressCtr.beValid;

			var  shieldNulable = shieldAddressCtr.shieldNulable;
			if (shieldNulable==null)
			{
				this.xpn1.txt = "";
			}
			else
			{
				var module = nilnul.fs.address_.shield.sups._NearestGitModuleX.ShieldNulable(shieldNulable);
				if (module==null)
				{
					xpn1.txt = "not in module";
				}
				else
				{
					xpn1.txt = "";
				}
			}
		}

		private void shieldAddressCtr_changedReportable()
		{
			on();

		}
	}
}