﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace nilnul.fs._git_._WIN_CTR_
{
	public partial class Module : UserControl
	{
		public event EventHandler txtChanged;
		public Module()
		{
			InitializeComponent();
		}

		private nilnul.fs.git.ModuleI _parsed;

		public nilnul.fs.git.ModuleI parsed => _parsed;



		public bool hasErr => !string.IsNullOrWhiteSpace(this.txtErr.Text);


		public string txt
		{
			get => this.textBox1.Text;
			set => this.textBox1.Text = value;
		}

		private void textBox1_TextChanged(object sender, EventArgs e)
		{
			validate();

			txtChanged?.Invoke(this, EventArgs.Empty);

		}

		private void validate()
		{
			string txtNormalized = textBox1.Text ?? "";

			string errMessages = "";

			//var parsed = new Dictionary<int, nilnul.fs.AddressI1>();


			try
			{
				_parsed = nilnul.fs.git.Module.FroAddress(txtNormalized);


			}
			catch (Exception e1)
			{
				errMessages =
					$"err when try to parse the address as module:{e1}"
				;
			}

			if (nilnul.txt.be_.NonWhite.Singleton.be(errMessages))
			{
				this.txtErr.Text = (errMessages);
				this.txtErr.BackColor = Color.Red;
			}
			else
			{
				this.txtErr.Text = "";
				this.txtErr.BackColor = Color.Gray;
			}
		}

		private void textBox1_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Control && e.KeyCode == Keys.A)
			{
				this.textBox1.SelectAll();
			}
		}

		private void Module_Load(object sender, EventArgs e)
		{
			validate();

			//txtChanged?.Invoke(this, EventArgs.Empty);

		}
	}
}
