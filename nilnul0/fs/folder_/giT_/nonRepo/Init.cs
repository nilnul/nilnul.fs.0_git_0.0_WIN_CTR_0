﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nilnul.fs.folder_.giT_.nonRepo
{
	public partial class Init : UserControl
	{
		public Init()
		{
			InitializeComponent();
			_validate();
		}

		void _validate() {
			if (this.fs_folder__gitT__NonRepo1.beOk)
			{
				button1init.Enabled = true;
				this.button1init.Text = "确定";
			}
			else
			{
				//this.button1init.Text = "input is invalid";
				this.button1init.Text = "确定";
				this.button1init.Enabled = false;
			}

		}
		void on() {
			
		}
		private void Button1init_Click(object sender, EventArgs e)
		{
			/// init the repo
			/// 
			nilnul.fs.git._module._IniX1.Ini(
				this.fs_folder__gitT__NonRepo1.val
			);

			nilnul.fs.git.module._TranscryptX.CreateInstaller(this.fs_folder__gitT__NonRepo1.val);

			nilnul.fs.git.module._TranscryptX.CreateBatch4Setup(this.fs_folder__gitT__NonRepo1.val);

			nilnul.fs.git.module._TranscryptX.RunInstaller(this.fs_folder__gitT__NonRepo1.val);


		}

		private void Button2refresh_Click(object sender, EventArgs e)
		{

		}

		private void Fs_folder__gitT__NonRepo1_report()
		{
			_validate();
		}
	}
}
