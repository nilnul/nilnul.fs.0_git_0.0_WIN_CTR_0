﻿namespace nilnul.fs.folder_.giT_.nonRepo
{
	partial class Init
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.button1init = new System.Windows.Forms.Button();
			this.button2refresh = new System.Windows.Forms.Button();
			this.fs_folder__gitT__NonRepo1 = new nilnul.fs.folder_.giT_.fs_folder__gitT__NonRepo();
			this.SuspendLayout();
			// 
			// button1init
			// 
			this.button1init.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.button1init.Location = new System.Drawing.Point(252, 294);
			this.button1init.Name = "button1init";
			this.button1init.Size = new System.Drawing.Size(75, 23);
			this.button1init.TabIndex = 1;
			this.button1init.Text = "初始化库";
			this.button1init.UseVisualStyleBackColor = true;
			this.button1init.Click += new System.EventHandler(this.Button1init_Click);
			// 
			// button2refresh
			// 
			this.button2refresh.Location = new System.Drawing.Point(157, 295);
			this.button2refresh.Name = "button2refresh";
			this.button2refresh.Size = new System.Drawing.Size(75, 23);
			this.button2refresh.TabIndex = 2;
			this.button2refresh.Text = "刷新";
			this.button2refresh.UseVisualStyleBackColor = true;
			this.button2refresh.Click += new System.EventHandler(this.Button2refresh_Click);
			// 
			// fs_folder__gitT__NonRepo1
			// 
			this.fs_folder__gitT__NonRepo1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.fs_folder__gitT__NonRepo1.beOk = false;
			this.fs_folder__gitT__NonRepo1.Location = new System.Drawing.Point(3, 3);
			this.fs_folder__gitT__NonRepo1.Name = "fs_folder__gitT__NonRepo1";
			this.fs_folder__gitT__NonRepo1.Size = new System.Drawing.Size(569, 285);
			this.fs_folder__gitT__NonRepo1.TabIndex = 3;
			this.fs_folder__gitT__NonRepo1.report += new System.Action(this.Fs_folder__gitT__NonRepo1_report);
			// 
			// Init
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.fs_folder__gitT__NonRepo1);
			this.Controls.Add(this.button2refresh);
			this.Controls.Add(this.button1init);
			this.Name = "Init";
			this.Size = new System.Drawing.Size(575, 325);
			this.ResumeLayout(false);

		}

		#endregion
		private System.Windows.Forms.Button button1init;
		private System.Windows.Forms.Button button2refresh;
		private fs_folder__gitT__NonRepo fs_folder__gitT__NonRepo1;
	}
}
