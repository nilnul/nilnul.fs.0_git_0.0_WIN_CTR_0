﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using nilnul.fs.folder;

namespace nilnul.fs.folder_.giT_
{
	public partial class fs_folder__gitT__NonRepo : UserControl
	{
		public event Action report;
		public fs_folder__gitT__NonRepo()
		{
			InitializeComponent();
			_validate();
		}

		private bool _beOk;

		public bool beOk
		{
			get { return _beOk; }
			set { _beOk = value; }
		}


		void _validate()
		{
			if (this.address__shield__address__Folder1.beOk)
			{
				if (
					nilnul.fs.folder.be_.git_.repo.Anto.Singleton.Be_ofAddress(this.address__shield__address__Folder1.val)
				)
				{
					this.eg__herit__readonly__multiline__xpn__trigValid_WithInput1.xpn = "";
				}
				else
				{
					eg__herit__readonly__multiline__xpn__trigValid_WithInput1.xpn = "folder is repo";
				}
			}
			else
			{
				eg__herit__readonly__multiline__xpn__trigValid_WithInput1.xpn = "";
			}
			_beOk = eg__herit__readonly__multiline__xpn__trigValid_WithInput1.xpnCtr.beOk && this.address__shield__address__Folder1.beOk;


		}

		/// <summary>
		/// based on history and currenct, determine nextstep.
		/// </summary>
		void decide()
		{
			var oldBeOk = _beOk;
			_validate();
			if (oldBeOk || beOk)
			{
				trig();
			}
		}

		private void trig()
		{
			report?.Invoke();

		}

		public string val {
			get {
				return this.address__shield__address__Folder1.val;
			}
		}
		private void Address__shield__address__Folder1_report()
		{
			decide();
		}
	}
}
