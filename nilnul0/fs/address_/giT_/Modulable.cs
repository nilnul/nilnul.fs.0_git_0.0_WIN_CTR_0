﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using nilnul.fs.folder;
using nilnul.fs.address;

namespace nilnul.fs.address_.giT_
{
	public partial class fs_address__gitT__Modulable : UserControl
	{
		public event Action report;
		public fs_address__gitT__Modulable()
		{
			InitializeComponent();
			_validate();
		}

		private bool _beOk;

		public bool beOk
		{
			get { return _beOk; }
			set { _beOk = value; }
		}


		void _validate()
		{
			if (this.address__shield__Address.beOk)
			{
				if (
					nilnul.fs.address.be_.giT_.Nonexist0FolderNonrepo.Singleton.Be(this.address__shield__Address.txt)
				)
				{
					this.eg__herit__readonly__multiline__xpn__trigValid_WithInput1.xpn = "";
				}
				else
				{
					eg__herit__readonly__multiline__xpn__trigValid_WithInput1.xpn = "it's a file or a repo folder";
				}
			}
			else
			{
				eg__herit__readonly__multiline__xpn__trigValid_WithInput1.xpn = "";
			}
			_beOk = eg__herit__readonly__multiline__xpn__trigValid_WithInput1.xpnCtr.beOk && this.address__shield__Address.beOk;


		}

		/// <summary>
		/// based on history and currenct, determine nextstep.
		/// </summary>
		void decide()
		{
			var oldBeOk = _beOk;
			_validate();
			if (oldBeOk || beOk)
			{
				trig();
			}
		}

		private void trig()
		{
			report?.Invoke();

		}

		public string val {
			get {
				return this.address__shield__Address.txt;
			}
		}
		private void Address__shield__address__Folder1_report()
		{
			decide();
		}
	}
}
