﻿namespace nilnul.fs.address_.giT_
{
	partial class fs_address__gitT__Modulable
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.eg__herit__readonly__multiline__xpn__trigValid_WithInput1 = new nilnul._txt_._WIN_CTR_.eg_.herit_.readonly_.multiline_.xpn_.trigValid.eg__herit__readonly__multiline__xpn__trigValid_WithInput();
			this.address__shield__Address = new nilnul._fs_._WIN_CTR_.address_.shield_.address__shield__Address();
			((System.ComponentModel.ISupportInitialize)(this.eg__herit__readonly__multiline__xpn__trigValid_WithInput1)).BeginInit();
			this.eg__herit__readonly__multiline__xpn__trigValid_WithInput1.Panel1.SuspendLayout();
			this.eg__herit__readonly__multiline__xpn__trigValid_WithInput1.SuspendLayout();
			this.SuspendLayout();
			// 
			// eg__herit__readonly__multiline__xpn__trigValid_WithInput1
			// 
			this.eg__herit__readonly__multiline__xpn__trigValid_WithInput1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.eg__herit__readonly__multiline__xpn__trigValid_WithInput1.Location = new System.Drawing.Point(0, 0);
			this.eg__herit__readonly__multiline__xpn__trigValid_WithInput1.Name = "eg__herit__readonly__multiline__xpn__trigValid_WithInput1";
			this.eg__herit__readonly__multiline__xpn__trigValid_WithInput1.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// eg__herit__readonly__multiline__xpn__trigValid_WithInput1.Panel1
			// 
			this.eg__herit__readonly__multiline__xpn__trigValid_WithInput1.Panel1.Controls.Add(this.address__shield__Address);
			this.eg__herit__readonly__multiline__xpn__trigValid_WithInput1.Panel2Collapsed = true;
			this.eg__herit__readonly__multiline__xpn__trigValid_WithInput1.Size = new System.Drawing.Size(825, 477);
			this.eg__herit__readonly__multiline__xpn__trigValid_WithInput1.TabIndex = 0;
			this.eg__herit__readonly__multiline__xpn__trigValid_WithInput1.xpn = "";
			// 
			// address__shield__Address
			// 
			this.address__shield__Address.Dock = System.Windows.Forms.DockStyle.Fill;
			this.address__shield__Address.Location = new System.Drawing.Point(0, 0);
			this.address__shield__Address.Name = "address__shield__Address";
			this.address__shield__Address.Size = new System.Drawing.Size(825, 477);
			this.address__shield__Address.TabIndex = 0;
			this.address__shield__Address.txt = "";
			this.address__shield__Address.changedReportable += new System.Action(this.Address__shield__address__Folder1_report);
			// 
			// fs_address__gitT__Modulable
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.eg__herit__readonly__multiline__xpn__trigValid_WithInput1);
			this.Name = "fs_address__gitT__Modulable";
			this.Size = new System.Drawing.Size(825, 477);
			this.eg__herit__readonly__multiline__xpn__trigValid_WithInput1.Panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.eg__herit__readonly__multiline__xpn__trigValid_WithInput1)).EndInit();
			this.eg__herit__readonly__multiline__xpn__trigValid_WithInput1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private _txt_._WIN_CTR_.eg_.herit_.readonly_.multiline_.xpn_.trigValid.eg__herit__readonly__multiline__xpn__trigValid_WithInput eg__herit__readonly__multiline__xpn__trigValid_WithInput1;
		private _fs_._WIN_CTR_.address_.shield_.address__shield__Address address__shield__Address;
	}
}
