﻿namespace nilnul.fs.address_.new0EmptyFolder.giT_
{
	partial class fs_address__new0EmptyFolder_gitT__Init
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.button1init = new System.Windows.Forms.Button();
			this.button2refresh = new System.Windows.Forms.Button();
			this.fs_address__gitT__Modulable1 = new nilnul._fs_._WIN_CTR_.address_.address__Nonexist0EmptyFolder();
			this.SuspendLayout();
			// 
			// button1init
			// 
			this.button1init.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.button1init.Location = new System.Drawing.Point(323, 410);
			this.button1init.Name = "button1init";
			this.button1init.Size = new System.Drawing.Size(75, 23);
			this.button1init.TabIndex = 1;
			this.button1init.Text = "初始化库";
			this.button1init.UseVisualStyleBackColor = true;
			this.button1init.Click += new System.EventHandler(this.Button1init_Click);
			// 
			// button2refresh
			// 
			this.button2refresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.button2refresh.Location = new System.Drawing.Point(4, 410);
			this.button2refresh.Name = "button2refresh";
			this.button2refresh.Size = new System.Drawing.Size(75, 23);
			this.button2refresh.TabIndex = 2;
			this.button2refresh.Text = "刷新";
			this.button2refresh.UseVisualStyleBackColor = true;
			this.button2refresh.Click += new System.EventHandler(this.Button2refresh_Click);
			// 
			// fs_address__gitT__Modulable1
			// 
			this.fs_address__gitT__Modulable1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.fs_address__gitT__Modulable1.beOk = false;
			this.fs_address__gitT__Modulable1.Location = new System.Drawing.Point(3, 3);
			this.fs_address__gitT__Modulable1.Name = "fs_address__gitT__Modulable1";
			this.fs_address__gitT__Modulable1.Size = new System.Drawing.Size(711, 401);
			this.fs_address__gitT__Modulable1.TabIndex = 3;
			this.fs_address__gitT__Modulable1.report += new System.Action(this.Fs_folder__gitT__NonRepo1_report);
			// 
			// fs_address__new0EmptyFolder_gitT__Init
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.fs_address__gitT__Modulable1);
			this.Controls.Add(this.button2refresh);
			this.Controls.Add(this.button1init);
			this.Name = "fs_address__new0EmptyFolder_gitT__Init";
			this.Size = new System.Drawing.Size(717, 441);
			this.ResumeLayout(false);

		}

		#endregion
		private System.Windows.Forms.Button button1init;
		private System.Windows.Forms.Button button2refresh;
		private nilnul._fs_._WIN_CTR_.address_.address__Nonexist0EmptyFolder fs_address__gitT__Modulable1;
	}
}
