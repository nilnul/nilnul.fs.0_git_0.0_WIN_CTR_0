﻿namespace nilnul.fs._git_._WIN_CTR_.svr.client_
{
	partial class svr_client__Github
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cred1 = new nilnul._acc_._CTR4WIN_.WinVault();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.svr__Header1 = new nilnul.fs._git_._WIN_CTR_.svr.client_._github_.Header();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.SuspendLayout();
			// 
			// cred1
			// 
			this.cred1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.cred1.Location = new System.Drawing.Point(0, 0);
			this.cred1.Name = "cred1";
			this.cred1.rsc = "";
			this.cred1.rscEditable = true;
			this.cred1.Size = new System.Drawing.Size(702, 163);
			this.cred1.TabIndex = 1;
			this.cred1.valName = "";
			this.cred1.valPass = "";
			this.cred1.Load += new System.EventHandler(this.Cred1_Load);
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.Location = new System.Drawing.Point(0, 0);
			this.splitContainer1.Name = "splitContainer1";
			this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.svr__Header1);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.cred1);
			this.splitContainer1.Size = new System.Drawing.Size(702, 331);
			this.splitContainer1.SplitterDistance = 164;
			this.splitContainer1.TabIndex = 2;
			// 
			// svr__Header1
			// 
			this.svr__Header1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.svr__Header1.Location = new System.Drawing.Point(0, 0);
			this.svr__Header1.Name = "svr__Header1";
			this.svr__Header1.Size = new System.Drawing.Size(702, 164);
			this.svr__Header1.TabIndex = 0;
			this.svr__Header1.val = "";
			this.svr__Header1.report += new System.Action(this.svr__Header1_report);
			this.svr__Header1.Load += new System.EventHandler(this._header_Load);
			// 
			// svr_client__Github
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.splitContainer1);
			this.Name = "svr_client__Github";
			this.Size = new System.Drawing.Size(702, 331);
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private _github_.Header svr__Header1;
		private _acc_._CTR4WIN_.WinVault cred1;
		private System.Windows.Forms.SplitContainer splitContainer1;
	}
}
