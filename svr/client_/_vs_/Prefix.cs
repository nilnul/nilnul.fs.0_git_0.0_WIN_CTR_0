﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nilnul.fs._git_._WIN_CTR_.svr_.vs
{
	/// <summary>
	/// org for dev.azure.com or visualstudio.com
	/// </summary>
	public partial class svr__vs_Prefix : UserControl

	{
		public event Action report;
		public svr__vs_Prefix()
		{
			InitializeComponent();

			//load();

		}

		public bool beOk {
			get {
				return this.eg__line__Herit1.beOk;
			}

		}
		public bool beErr {
			get {
				return eg__line__Herit1.beErr;
			}
		}

		public void load() {
			if (!nilnul.fs.git.Properties.Settings.Default.upgraded)
			{
				nilnul.fs.git.Properties.Settings.Default.Upgrade();
				nilnul.fs.git.Properties.Settings.Default.upgraded = true;
				nilnul.fs.git.Properties.Settings.Default.Save();      //save the above "upgraded"
			}


			this.eg__line__Herit1.val = nilnul.fs.git.Properties.Settings.Default.svr_vs__prefix??"";

		}


		private void Eg__line__Herit1_TextChanged(object sender, EventArgs e)
		{
			//if (this.eg__line__Herit1.txt is valid)
			//{

			//}

			//this.eg__herit__readonly__multiline__xpn__trigValid_WithInput11.xpn = "";
		}

		public string val {
			get { return this.eg__line__Herit1.val; }
			set {
				this.eg__line__Herit1.val = value;
			}
		}

		private void eg__line__Herit1_report()
		{
			trig();
		}

		private void trig()
		{
			report?.Invoke();
		}
	}
}
