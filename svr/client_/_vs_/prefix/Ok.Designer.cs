﻿namespace nilnul.fs._git_._WIN_CTR_.svr_.vs.prefix
{
	partial class svr__vs_prefix_Ok
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.ok1 = new _nilnul_._CTR4WIN_.Ok();
			this.svr__vs_Prefix1 = new nilnul.fs._git_._WIN_CTR_.svr_.vs.svr__vs_Prefix();
			((System.ComponentModel.ISupportInitialize)(this.ok1)).BeginInit();
			this.ok1.Panel1.SuspendLayout();
			this.ok1.SuspendLayout();
			this.SuspendLayout();
			// 
			// ok1
			// 
			this.ok1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ok1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
			this.ok1.Location = new System.Drawing.Point(0, 0);
			this.ok1.Name = "ok1";
			this.ok1.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// ok1.Panel1
			// 
			this.ok1.Panel1.Controls.Add(this.svr__vs_Prefix1);
			// 
			// ok1.Panel2
			// 
			this.ok1.Panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.ok1_Panel2_Paint);
			this.ok1.Size = new System.Drawing.Size(483, 358);
			this.ok1.SplitterDistance = 308;
			this.ok1.TabIndex = 0;
			this.ok1.oked += new System.Action(this.ok1_oked_1);
			// 
			// svr__vs_Prefix1
			// 
			this.svr__vs_Prefix1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.svr__vs_Prefix1.Location = new System.Drawing.Point(0, 0);
			this.svr__vs_Prefix1.Name = "svr__vs_Prefix1";
			this.svr__vs_Prefix1.Size = new System.Drawing.Size(483, 308);
			this.svr__vs_Prefix1.TabIndex = 0;
			this.svr__vs_Prefix1.val = "";
			this.svr__vs_Prefix1.Load += new System.EventHandler(this.svr__vs_Prefix1_Load);
			// 
			// svr__vs_prefix_Ok
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.ok1);
			this.Name = "svr__vs_prefix_Ok";
			this.Size = new System.Drawing.Size(483, 358);
			this.ok1.Panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.ok1)).EndInit();
			this.ok1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private _nilnul_._CTR4WIN_.Ok ok1;
		private svr__vs_Prefix svr__vs_Prefix1;
	}
}
