﻿namespace nilnul.fs._git_._WIN_CTR_.svr_.vs.prefix.ok
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.svr__vs_prefix_Ok1 = new nilnul.fs._git_._WIN_CTR_.svr_.vs.prefix.svr__vs_prefix_Ok();
			this.SuspendLayout();
			// 
			// svr__vs_prefix_Ok1
			// 
			this.svr__vs_prefix_Ok1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.svr__vs_prefix_Ok1.Location = new System.Drawing.Point(0, 0);
			this.svr__vs_prefix_Ok1.Name = "svr__vs_prefix_Ok1";
			this.svr__vs_prefix_Ok1.prefix = "";
			this.svr__vs_prefix_Ok1.Size = new System.Drawing.Size(800, 450);
			this.svr__vs_prefix_Ok1.TabIndex = 0;
			this.svr__vs_prefix_Ok1.oked += new System.Action(this.svr__vs_prefix_Ok1_oked);
			this.svr__vs_prefix_Ok1.Load += new System.EventHandler(this.svr__vs_prefix_Ok1_Load);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(800, 450);
			this.Controls.Add(this.svr__vs_prefix_Ok1);
			this.Name = "Form1";
			this.Text = "svr_.vs.prefix";
			this.ResumeLayout(false);

		}

		#endregion

		private svr__vs_prefix_Ok svr__vs_prefix_Ok1;
	}
}