﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nilnul.fs._git_._WIN_CTR_.svr_.vs.prefix.ok
{
	public partial class Form1 : Form
	{
		public event Action oked;
		public Form1()
		{
			InitializeComponent();
		}

		private void svr__vs_prefix_Ok1_oked()
		{
			nilnul.fs.git.Properties.Settings.Default.svr_vs__prefix = this.svr__vs_prefix_Ok1.prefix;
			nilnul.fs.git.Properties.Settings.Default.Save();
			oked?.Invoke();
		}

		private void svr__vs_prefix_Ok1_Load(object sender, EventArgs e)
		{
			this.svr__vs_prefix_Ok1.load();
		}
	}
}
