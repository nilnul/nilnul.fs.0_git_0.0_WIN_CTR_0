﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nilnul.fs._git_._WIN_CTR_.svr.client_
{
	/// <summary>
	/// visualstudio.com dev
	/// </summary>
	public partial class svr_client__Vs : UserControl,_client_.SaveCredI
	{
		public svr_client__Vs()
		{
			InitializeComponent();
		}

		public string prefix
		{
			get
			{
				return this.svr__vs_Prefix1.val;
			}
			set
			{
				this.svr__vs_Prefix1.val = value;
			}
		}


		public bool beOk {
			get {

				return this.svr__vs_Prefix1.beOk;

			}
		}



		public nilnul.fs.git.svr_.vs.client_.AccInVault accVaulted
		{
			get
			{
				return new git.svr_.vs.client_.AccInVault(this.prefix);
			}
			set
			{
				this.prefix = value.prefix;
			}
		}



		private void Svr__vs_Prefix1_Load(object sender, EventArgs e)
		{
			//this.svr__vs_Prefix1.load();


		}
		private void Cred1_Load()
		{
			this.cred1.loadRsc(
				accVaulted.credKey4push
				);

		}
		private void Cred1_Load(object sender, EventArgs e)
		{
			Cred1_Load();

			//if (
			//	nilnul.txt.nulable.be_.NeitherNulNorWhite.Singleton.be(
			//		nilnul.fs.git.Properties.Settings.Default.svr_vs__prefix
			//	)
			//)
			//{
			//	this.cred1.loadRsc(
			//		nilnul.fs.git.svr_.vs_.prefixed.client_._CfgedX.CredRsc
			//	);
			//}

		}



		public void saveCred() {
			

			this.cred1.save();

		}

		private void svr__vs_Prefix1_report()
		{
			if (this.svr__vs_Prefix1.beOk)
			{
				Cred1_Load();
			}
		}
	}
}
