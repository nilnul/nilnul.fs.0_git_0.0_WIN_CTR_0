﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nilnul.fs._git_._WIN_CTR_.svr.client_._github_
{
	[Obsolete(nameof(_WIN_CTR_.svr_.github._client.svr__github__client_Agent))]
	public partial class Header : UserControl
	{
		public Header()
		{
			InitializeComponent();
		}

		private void name_Labeled1_Load(object sender, EventArgs e)
		{
			this.name_Labeled1.lableTxt = _header.Resource1.labelTxt;
		}

		public string val {
			get {
				return this.name_Labeled1.val;
			}
			set {
				this.name_Labeled1.val = value;
			}
		}

		public bool beOk {
			get {
				return this.name_Labeled1.beOk;
			}
		}
		public bool beErr {
			get {
				return this.name_Labeled1.beErr;
			}
		}

		public event Action report;
		private void name_Labeled1_report()
		{
			report?.Invoke();
		}
	}
}
