﻿
namespace nilnul.fs._git_._WIN_CTR_.svr.client_._github_
{
	partial class Header
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.name_Labeled1 = new nilnul._acc_._CTR4WIN_.name.name_Labeled();
			this.SuspendLayout();
			// 
			// name_Labeled1
			// 
			this.name_Labeled1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.name_Labeled1.lableTxt = "产品：";
			this.name_Labeled1.Location = new System.Drawing.Point(0, 0);
			this.name_Labeled1.Name = "name_Labeled1";
			this.name_Labeled1.Size = new System.Drawing.Size(273, 191);
			this.name_Labeled1.TabIndex = 0;
			this.name_Labeled1.val = "";
			this.name_Labeled1.report += new System.Action(this.name_Labeled1_report);
			this.name_Labeled1.Load += new System.EventHandler(this.name_Labeled1_Load);
			// 
			// Header
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.name_Labeled1);
			this.Name = "Header";
			this.Size = new System.Drawing.Size(273, 191);
			this.ResumeLayout(false);

		}

		#endregion

		private _acc_._CTR4WIN_.name.name_Labeled name_Labeled1;
	}
}
