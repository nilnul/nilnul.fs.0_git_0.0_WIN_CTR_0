﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nilnul.fs._git_._WIN_CTR_.svr.client_
{
	[Obsolete(nameof(fs._git_._WIN_CTR_.svr_.github.client_))]
	public partial class svr_client__Github : UserControl,
		_client_.SaveCredI
	{
		public svr_client__Github()
		{
			InitializeComponent();
		}

		public string header
		{
			get
			{
				return this.svr__Header1.val;
			}
			set
			{
				this.svr__Header1.val = value;
			}
		}

		public nilnul.fs.git.svr.client_.accVaulted_.Github accVaulted {
			get {
				return new git.svr.client_.accVaulted_.Github(this.header);
			}
			set {
				this.header = value.header;
			}
		}


		private void _header_Load(object sender, EventArgs e)
		{
			//this.svr__vs_Prefix1.load();
		}

		private void Cred1_Load()
		{
				this.cred1.loadRsc(

					nilnul.fs.git.svr_.github.client_.AccInVault.VaultKey()

					//"github.com"
					//nilnul.fs.git.svr_.Github.Singleton.host
				);
		}
		private void Cred1_Load(object sender, EventArgs e)
		{
			Cred1_Load();
		}

		public void save() {
			//nilnul.fs.git.Properties.Settings.Default.svr_vs__prefix = this.svr__vs_Prefix1.val;
			//nilnul.fs.git.Properties.Settings.Default.Save();
			this.cred1.save();
		}

		private void svr__Header1_report()
		{
			Cred1_Load();
		}

		public void saveCred()
		{
			//nilnul.fs.git.Properties.Settings.Default.svr_vs__prefix = this.svr__amazon__Region1.val;


			//nilnul.fs.git.Properties.Settings.Default.Save();

			this.cred1.save();

		}
	}
}
