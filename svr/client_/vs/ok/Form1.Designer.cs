﻿namespace nilnul.fs._git_._WIN_CTR_.svr_._vs_._ok_
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.svr___vs__Ok1 = new nilnul.fs._git_._WIN_CTR_.svr_._vs_.svr___vs__Ok();
			this.SuspendLayout();
			// 
			// svr___vs__Ok1
			// 
			this.svr___vs__Ok1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.svr___vs__Ok1.Location = new System.Drawing.Point(0, 0);
			this.svr___vs__Ok1.Name = "svr___vs__Ok1";
			this.svr___vs__Ok1.Size = new System.Drawing.Size(800, 450);
			this.svr___vs__Ok1.TabIndex = 0;
			this.svr___vs__Ok1.Load += new System.EventHandler(this.Svr___vs__Ok1_Load);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(800, 450);
			this.Controls.Add(this.svr___vs__Ok1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);

		}

		#endregion

		private svr___vs__Ok svr___vs__Ok1;
	}
}