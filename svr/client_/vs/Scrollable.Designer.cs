﻿
namespace nilnul.fs._git_._WIN_CTR_.svr.client_.vs
{
	partial class Scrollable
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Scrollable));
            this.svr_client__Vs1 = new nilnul.fs._git_._WIN_CTR_.svr.client_.svr_client__Vs();
            this.SuspendLayout();
            // 
            // svr_client__Vs1
            // 
            this.svr_client__Vs1.accVaulted = ((nilnul.fs.git.svr_.vs.client_.AccInVault)(resources.GetObject("svr_client__Vs1.accVaulted")));
            this.svr_client__Vs1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.svr_client__Vs1.Location = new System.Drawing.Point(0, 0);
            this.svr_client__Vs1.MinimumSize = new System.Drawing.Size(409, 129);
            this.svr_client__Vs1.Name = "svr_client__Vs1";
            this.svr_client__Vs1.prefix = "";
            this.svr_client__Vs1.Size = new System.Drawing.Size(452, 144);
            this.svr_client__Vs1.TabIndex = 0;
            // 
            // Scrollable
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoScrollMinSize = new System.Drawing.Size(409, 129);
            this.Controls.Add(this.svr_client__Vs1);
            this.Name = "Scrollable";
            this.Size = new System.Drawing.Size(452, 144);
            this.ResumeLayout(false);

		}

		#endregion

		private svr_client__Vs svr_client__Vs1;
	}
}
