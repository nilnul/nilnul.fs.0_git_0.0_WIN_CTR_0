﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nilnul.fs._git_._WIN_CTR_.svr_._vs_
{
	public partial class svr___vs__Ok : UserControl
	{
		public event Action oked;
		public svr___vs__Ok()
		{
			InitializeComponent();
		}

		private void Button1_Click(object sender, EventArgs e)
		{
			this.svr__Vs1.saveCred();
			oked?.Invoke();
			//nilnul.fs.git.Properties.Settings.Default.svr_vs__prefix = this.svr__Vs1.prefix;
			//nilnul.fs.git.Properties.Settings.Default.Save();
			//button1.Text = $"Saved at {DateTime.Now}";
		}
	}
}
