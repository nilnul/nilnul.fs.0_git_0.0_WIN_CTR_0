﻿namespace nilnul.fs._git_._WIN_CTR_.svr_._vs_.ok
{
	partial class svr__vs__ok_Notice
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.alert1 = new _nilnul_._CTR4WIN_.Alert();
			this.svr___vs__Ok1 = new nilnul.fs._git_._WIN_CTR_.svr_._vs_.svr___vs__Ok();
			this.SuspendLayout();
			// 
			// alert1
			// 
			this.alert1.Location = new System.Drawing.Point(98, 94);
			this.alert1.msg = "";
			this.alert1.Name = "alert1";
			this.alert1.Size = new System.Drawing.Size(218, 148);
			this.alert1.TabIndex = 0;
			// 
			// svr___vs__Ok1
			// 
			this.svr___vs__Ok1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.svr___vs__Ok1.Location = new System.Drawing.Point(0, 0);
			this.svr___vs__Ok1.Name = "svr___vs__Ok1";
			this.svr___vs__Ok1.Size = new System.Drawing.Size(571, 466);
			this.svr___vs__Ok1.TabIndex = 1;
			// 
			// Notice
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.svr___vs__Ok1);
			this.Controls.Add(this.alert1);
			this.Name = "Notice";
			this.Size = new System.Drawing.Size(571, 466);
			this.ResumeLayout(false);

		}

		#endregion

		private _nilnul_._CTR4WIN_.Alert alert1;
		private svr___vs__Ok svr___vs__Ok1;
	}
}
