﻿namespace nilnul.fs._git_._WIN_CTR_.svr.client_
{
	partial class svr_client__Amazon
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.credRsced4Create = new nilnul._acc_._CTR4WIN_.winVault_.RscUneditable();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.svr__amazon__Region1 = new nilnul.fs._git_._WIN_CTR_.svr.client_._amazon_.svr___amazon__Region();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.credRsced4Push = new nilnul._acc_._CTR4WIN_.cred.rsced.vaulted_.cred_rsced_vaulted__RscUneditable();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.SuspendLayout();
			// 
			// credRsced4Create
			// 
			this.credRsced4Create.Dock = System.Windows.Forms.DockStyle.Fill;
			this.credRsced4Create.Location = new System.Drawing.Point(3, 3);
			this.credRsced4Create.Name = "credRsced4Create";
			this.credRsced4Create.rsc = "";
			this.credRsced4Create.rscEditable = false;
			this.credRsced4Create.Size = new System.Drawing.Size(619, 188);
			this.credRsced4Create.TabIndex = 1;
			this.credRsced4Create.valName = "";
			this.credRsced4Create.valPass = "";
			this.credRsced4Create.Load += new System.EventHandler(this.Cred4client_Load);
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.Location = new System.Drawing.Point(0, 0);
			this.splitContainer1.Name = "splitContainer1";
			this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.svr__amazon__Region1);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.tabControl1);
			this.splitContainer1.Size = new System.Drawing.Size(633, 373);
			this.splitContainer1.SplitterDistance = 149;
			this.splitContainer1.TabIndex = 2;
			// 
			// svr__amazon__Region1
			// 
			this.svr__amazon__Region1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.svr__amazon__Region1.Location = new System.Drawing.Point(0, 0);
			this.svr__amazon__Region1.Name = "svr__amazon__Region1";
			this.svr__amazon__Region1.Size = new System.Drawing.Size(633, 149);
			this.svr__amazon__Region1.TabIndex = 0;
			this.svr__amazon__Region1.val = "af-south-1";
			this.svr__amazon__Region1.changed += new System.Action(this.svr__amazon__Region1_changed);
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.Location = new System.Drawing.Point(0, 0);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(633, 220);
			this.tabControl1.TabIndex = 0;
			this.tabControl1.Tag = "Client";
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.credRsced4Create);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage1.Size = new System.Drawing.Size(625, 194);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "Client4create";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.credRsced4Push);
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage2.Size = new System.Drawing.Size(625, 194);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "Git4push";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// credRsced4Push
			// 
			this.credRsced4Push.Dock = System.Windows.Forms.DockStyle.Fill;
			this.credRsced4Push.Location = new System.Drawing.Point(3, 3);
			this.credRsced4Push.Name = "credRsced4Push";
			this.credRsced4Push.password = "";
			this.credRsced4Push.rsc = "";
			this.credRsced4Push.rscEditable = false;
			this.credRsced4Push.Size = new System.Drawing.Size(619, 188);
			this.credRsced4Push.TabIndex = 0;
			this.credRsced4Push.username = "";
			this.credRsced4Push.Load += new System.EventHandler(this.cred4push_Load);
			// 
			// svr_client__Amazon
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.splitContainer1);
			this.Name = "svr_client__Amazon";
			this.Size = new System.Drawing.Size(633, 373);
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.tabControl1.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage2.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion
		private _acc_._CTR4WIN_.winVault_.RscUneditable credRsced4Create;
		private System.Windows.Forms.SplitContainer splitContainer1;
		private _amazon_.svr___amazon__Region svr__amazon__Region1;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabPage tabPage2;
		private _acc_._CTR4WIN_.cred.rsced.vaulted_.cred_rsced_vaulted__RscUneditable credRsced4Push;
	}
}
