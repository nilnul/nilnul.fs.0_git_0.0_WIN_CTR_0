﻿namespace nilnul.fs._git_._WIN_CTR_.svr.client_
{
	partial class svr_client__Gitlab
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.rscUneditable1 = new nilnul._acc_._CTR4WIN_.winVault_.RscUneditable();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.username_Labeled1 = new nilnul._acc_._CTR4WIN_.name.name_Labeled();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.SuspendLayout();
			// 
			// cred1
			// 
			this.rscUneditable1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.rscUneditable1.Location = new System.Drawing.Point(0, 0);
			this.rscUneditable1.Name = "cred1";
			this.rscUneditable1.rsc = "";
			this.rscUneditable1.rscEditable = false;
			this.rscUneditable1.Size = new System.Drawing.Size(702, 163);
			this.rscUneditable1.TabIndex = 1;
			this.rscUneditable1.valName = "";
			this.rscUneditable1.valPass = "";
			this.rscUneditable1.Load += new System.EventHandler(this.Cred1_Load);
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.Location = new System.Drawing.Point(0, 0);
			this.splitContainer1.Name = "splitContainer1";
			this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.username_Labeled1);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.rscUneditable1);
			this.splitContainer1.Size = new System.Drawing.Size(702, 331);
			this.splitContainer1.SplitterDistance = 164;
			this.splitContainer1.TabIndex = 2;
			// 
			// username_Labeled1
			// 
			this.username_Labeled1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.username_Labeled1.Location = new System.Drawing.Point(0, 0);
			this.username_Labeled1.Name = "username_Labeled1";
			this.username_Labeled1.Size = new System.Drawing.Size(702, 164);
			this.username_Labeled1.TabIndex = 0;
			this.username_Labeled1.val = "";
			this.username_Labeled1.report += new System.Action(this.username_Labeled1_report);
			// 
			// svr_client__Gitlab
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.splitContainer1);
			this.Name = "svr_client__Gitlab";
			this.Size = new System.Drawing.Size(702, 331);
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion
		private _acc_._CTR4WIN_.winVault_.RscUneditable rscUneditable1;
		private System.Windows.Forms.SplitContainer splitContainer1;
		private _acc_._CTR4WIN_.name.name_Labeled username_Labeled1;
	}
}
