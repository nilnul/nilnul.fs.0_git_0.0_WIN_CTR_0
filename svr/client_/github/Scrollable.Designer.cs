﻿
namespace nilnul.fs._git_._WIN_CTR_.svr.client_.github
{
	partial class Scrollable
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Scrollable));
            this.svr_client__Github1 = new nilnul.fs._git_._WIN_CTR_.svr.client_.svr_client__Github();
            this.SuspendLayout();
            // 
            // svr_client__Github1
            // 
            this.svr_client__Github1.accVaulted = ((nilnul.fs.git.svr.client_.accVaulted_.Github)(resources.GetObject("svr_client__Github1.accVaulted")));
            this.svr_client__Github1.header = "";
            this.svr_client__Github1.Location = new System.Drawing.Point(0, 3);
            this.svr_client__Github1.MinimumSize = new System.Drawing.Size(408, 130);
            this.svr_client__Github1.Name = "svr_client__Github1";
            this.svr_client__Github1.Size = new System.Drawing.Size(408, 130);
            this.svr_client__Github1.TabIndex = 0;
            // 
            // Scrollable
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoScrollMinSize = new System.Drawing.Size(408, 130);
            this.Controls.Add(this.svr_client__Github1);
            this.Name = "Scrollable";
            this.Size = new System.Drawing.Size(416, 140);
            this.ResumeLayout(false);

		}

		#endregion

		private svr_client__Github svr_client__Github1;
	}
}
