﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nilnul.fs._git_._WIN_CTR_.svr.client_
{
	public partial class svr_client__Amazon : UserControl,_client_.SaveCredI
	{
		public event Action report;
		public svr_client__Amazon()
		{
			InitializeComponent();
			Cred4client_Load();
		}

		public bool beOk {
			get {
				return true;
			}
		}

		public string region
		{
			get
			{
				return this.svr__amazon__Region1.val;
			}
			set
			{
				this.svr__amazon__Region1.val = value;
			}
		}

		public nilnul.fs.git.svr_.amazon.client_.AccInVault accVaulted
		{
			get
			{
				return new git.svr_.amazon.client_.AccInVault(this.region);
			}
			set
			{
				this.region = value.region;
			}
		}
		private void Cred4client_Load()
		{

			this.credRsced4Create.loadRsc(
				nilnul.fs.git.svr_.amazon.client_.AccInVault.VaultKey4client(this.region)
				//nilnul.fs.git.svr_.amazon_._RegionX.Domain(this.region)
			);

	
		}

		private void Cred4client_Load(object sender, EventArgs e)
		{
			Cred4client_Load();
			//this.cred1.loadRsc(
			//	nilnul.fs.git.svr_.amazon_._RegionX.Domain(this.region)
			//);

			//if (
			//	nilnul.txt.nulable.be_.NeitherNulNorWhite.Singleton.be(
			//		nilnul.fs.git.Properties.Settings.Default.svr_amazon__region
			//	)
			//)
			//{
			//}
		}




		private void Cred4push_Load()
		{

			this.credRsced4Push.loadRsc(
				nilnul.fs.git.svr_.amazon.client_.AccInVault.VaultKey4push(this.region)
				//nilnul.fs.git.svr_.amazon_._RegionX.Domain(this.region)
			);

	
		}
		private void cred4push_Load(object sender, EventArgs e)
		{
			Cred4push_Load();
		}

		public void saveCred() {
			//nilnul.fs.git.Properties.Settings.Default.svr_vs__prefix = this.svr__amazon__Region1.val;


			//nilnul.fs.git.Properties.Settings.Default.Save();

			this.credRsced4Create.save();
			this.credRsced4Push.save();

		}

		private void svr__amazon__Region1_changed()
		{
			Cred4client_Load();
			Cred4push_Load();
			report?.Invoke();
		}

	}
}
