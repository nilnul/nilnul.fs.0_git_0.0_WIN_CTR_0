﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nilnul.fs._git_._WIN_CTR_.svr.client_
{
	public partial class svr_client__Gitlab : UserControl,_client_.SaveCredI
	{
		public event Action report;
		public svr_client__Gitlab()
		{
			InitializeComponent();
		}

		public bool beOk {
			get {
				return this.username_Labeled1.beOk;
			}
		}
		public bool beErr {
			get {
				return this.username_Labeled1.beErr;
			}
		}

		public string username
		{
			get
			{
				return this.username_Labeled1.val;
			}
			set
			{
				this.username_Labeled1.val = value;
			}
		}

		public nilnul.fs.git.svr_.gitlab.client_.AccInVault accVaulted {
			get {
				return new git.svr_.gitlab.client_.AccInVault(this.username);
			}
			set {
				this.username = value.accName;
			}
		}



		private void Svr__vs_Prefix1_Load(object sender, EventArgs e)
		{
			//this.svr__vs_Prefix1.load();
		}

		private void Cred1_Load(object sender, EventArgs e)
		{
				this.rscUneditable1.loadRsc(
					//"github.com"
					accVaulted.credKey4push
				);
		}

		public void saveCred() {
			//nilnul.fs.git.Properties.Settings.Default.svr_vs__prefix = this.svr__vs_Prefix1.val;
			//nilnul.fs.git.Properties.Settings.Default.Save();
			this.rscUneditable1.save();
		}

		private void username_Labeled1_report()
		{
			// if the username is invalid, due to ,say , being empty,

			///then human is expected to revise the username while the rsc will not change.

			if (username_Labeled1.beOk)
			{
				this.rscUneditable1.loadRsc(
					//"github.com"
					accVaulted.credKey4push
				);
			}

			report?.Invoke();

		}
	}
}
