﻿
namespace nilnul.fs._git_._WIN_CTR_.svr.client_.amazon.scrollable.ok
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.svr_client__amazon_scrollable_Ok1 = new nilnul.fs._git_._WIN_CTR_.svr.client_.amazon.scrollable.svr_client__amazon_scrollable_Ok();
            this.SuspendLayout();
            // 
            // svr_client__amazon_scrollable_Ok1
            // 
            this.svr_client__amazon_scrollable_Ok1.accVaulted = ((nilnul.fs.git.svr_.amazon.client_.AccInVault)(resources.GetObject("svr_client__amazon_scrollable_Ok1.accVaulted")));
            this.svr_client__amazon_scrollable_Ok1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.svr_client__amazon_scrollable_Ok1.Location = new System.Drawing.Point(0, 0);
            this.svr_client__amazon_scrollable_Ok1.Name = "svr_client__amazon_scrollable_Ok1";
            this.svr_client__amazon_scrollable_Ok1.Size = new System.Drawing.Size(551, 407);
            this.svr_client__amazon_scrollable_Ok1.TabIndex = 0;
            this.svr_client__amazon_scrollable_Ok1.oked += new System.Action(this.ok1_oked);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(551, 407);
            this.Controls.Add(this.svr_client__amazon_scrollable_Ok1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

		}

		#endregion

		private svr_client__amazon_scrollable_Ok svr_client__amazon_scrollable_Ok1;
	}
}