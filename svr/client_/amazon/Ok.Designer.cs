﻿
namespace nilnul.fs._git_._WIN_CTR_.svr.client_.amazon
{
	partial class svr_client__amazon_Ok
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.ok1 = new _nilnul_._CTR4WIN_.Ok();
            ((System.ComponentModel.ISupportInitialize)(this.ok1)).BeginInit();
            this.ok1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ok1
            // 
            this.ok1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ok1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.ok1.Location = new System.Drawing.Point(0, 0);
            this.ok1.Name = "ok1";
            this.ok1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.ok1.Size = new System.Drawing.Size(280, 268);
            this.ok1.SplitterDistance = 218;
            this.ok1.TabIndex = 0;
            // 
            // svr_client__amazon_Ok
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ok1);
            this.Name = "svr_client__amazon_Ok";
            this.Size = new System.Drawing.Size(280, 268);
            ((System.ComponentModel.ISupportInitialize)(this.ok1)).EndInit();
            this.ok1.ResumeLayout(false);
            this.ResumeLayout(false);

		}

		#endregion

		private _nilnul_._CTR4WIN_.Ok ok1;
	}
}
