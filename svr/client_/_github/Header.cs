﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nilnul.fs._git_._WIN_CTR_.svr.client_._github
{
	/// <summary>
	/// git hub requires a header
	/// </summary>
	public partial class svr_client___github_Header : UserControl
	{
		public event Action changed;

		public event Action report;
		public svr_client___github_Header()
		{
			InitializeComponent();
		}

		//public void load() {
		//	if (!nilnul.fs.git.Properties.Settings.Default.upgraded)
		//	{
		//		nilnul.fs.git.Properties.Settings.Default.Upgrade();
		//		nilnul.fs.git.Properties.Settings.Default.upgraded = true;
		//		nilnul.fs.git.Properties.Settings.Default.Save();      //save the above "upgraded"
		//	}
		//	this.eg__line__Herit1required.val= nilnul.fs.git.Properties.Settings.Default.svr_vs__prefix??"";
		//}


		public string val {
			get {
				return this.eg__line__Herit1required.val;
			}
			set {
				this.eg__line__Herit1required.val = value;
			}
		}

		private void Eg__line__Herit1_Load(object sender, EventArgs e)
		{
		}

		void _validate() {
			if (this.eg__line__Herit1required.beOk)
			{
				//nonWhite input
				this.eg__herit__readonly__multiline__xpn__trigValid_WithInput11.xpn = "";
			}
			else
			{
				this.eg__herit__readonly__multiline__xpn__trigValid_WithInput11.xpn = "";
			}
		}

		public bool beOk {
			get {
				return this.eg__herit__readonly__multiline__xpn__trigValid_WithInput11.beOk && this.eg__line__Herit1required.beOk;
			}
		}

		public bool beErr {
			get {
				return !beOk;
			}
		}



		private void Eg__line__Herit1required_changed()
		{
			_validate();
			changed?.Invoke();
		}
	}
}
