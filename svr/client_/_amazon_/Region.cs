﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Amazon;
using nilnul.obj.str;

namespace nilnul.fs._git_._WIN_CTR_.svr.client_._amazon_
{
	public partial class svr___amazon__Region : UserControl
	{
		public event Action changed;
		public svr___amazon__Region()
		{
			InitializeComponent();
			bind();
		}

		public string val {
			get {



				return (this.comboBox1.SelectedItem as RegionEndpoint)?.SystemName;// ..Text;
			}
			set {
				if (value is null)
				{
					return;
				}

				var endPoint = RegionEndpoint.GetBySystemName(value);// value;

				var index=this.comboBox1.Items.Cast<RegionEndpoint>().FindIndex(r => r.SystemName == value);
				if (index>=0)
				{
					this.comboBox1.SelectedIndex = index;
					//this.comboBox1.Items[index]=RegionEndpoint.GetBySystemName(value);

				}
				

				
			}
		}
		public void bind() {
			this.comboBox1.DataSource = RegionEndpoint.EnumerableAllRegions.ToList();
		}

		public RegionEndpoint endpoint {
			get {
				return RegionEndpoint.GetBySystemName(val);
			}
		}

		public void load()
		{

			//this.comboBox1.SelectedValue= nilnul.fs.git.Properties.Settings.Default.svr_amazon__region ?? "";

		}

		private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
		{
			changed?.Invoke();
		}
	}
}
