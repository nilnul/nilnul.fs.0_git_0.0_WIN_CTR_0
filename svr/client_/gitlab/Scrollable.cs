﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nilnul.fs._git_._WIN_CTR_.svr.client_.gitlab
{
	public partial class svr_client__gitlab_Scrollable : UserControl
	{
		public svr_client__gitlab_Scrollable()
		{
			InitializeComponent();
		}

		public nilnul.fs.git.svr_.gitlab.client_.AccInVault accVaulted
		{
			get
			{
				return this.svr_client__Gitlab1.accVaulted;
			}
			set
			{
				this.svr_client__Gitlab1.accVaulted = value;
			}
		}

		public void saveCred()
		{
			this.svr_client__Gitlab1.saveCred();
		}
	}
}
