﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nilnul.fs._git_._WIN_CTR_.svr.client_.gitlab
{
	public partial class svr__gitlab_Ok : UserControl
	{
		public event Action oked;
		public svr__gitlab_Ok()
		{
			InitializeComponent();
		}

		public string header {
			get {
				return this.svr__Github1.username;
			}
			set {
				this.svr__Github1.username = value;
			}
		}

		public nilnul.fs.git.client.noCred_.Github val {
			get {
				return new git.client.noCred_.Github() { header= this.header};
			}
			set {
				this.header = value.header;
			}
		}

		private void Button1_Click(object sender, EventArgs e)
		{
			this.svr__Github1.save();		//save cred


			oked?.Invoke();
			//nilnul.fs.git.Properties.Settings.Default.svr_vs__prefix = this.svr__Vs1.prefix;
			//nilnul.fs.git.Properties.Settings.Default.Save();
			//button1.Text = $"Saved at {DateTime.Now}";
		}
	}
}
