﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nilnul.fs._git_._WIN_CTR_.svr.client_.gitlab.scrollable.ok
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		public event Action oked;
		private void ok1_oked()
		{
			// cred already saved
			this.DialogResult = DialogResult.OK;
			this.Close();
			oked?.Invoke();

		}

		public nilnul.fs.git.svr_.gitlab.client_.AccInVault accVaulted
		{
			get
			{
				return this.svr_client__gitlab_scrollable_Ok1.accVaulted;
			}
			set
			{
				this.svr_client__gitlab_scrollable_Ok1.accVaulted = value;
			}
		}
	}
}
