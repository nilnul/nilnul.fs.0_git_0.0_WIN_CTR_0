﻿namespace nilnul.fs._git_._WIN_CTR_.svr.client_.gitlab
{
	partial class svr__gitlab_Ok
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.button1 = new System.Windows.Forms.Button();
			this.svr__Github1 = new svr_client__Gitlab();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.button1.Location = new System.Drawing.Point(188, 198);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 1;
			this.button1.Text = "Save";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.Button1_Click);
			// 
			// svr__Vs1
			// 
			this.svr__Github1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.svr__Github1.Location = new System.Drawing.Point(3, 3);
			this.svr__Github1.Name = "svr__Vs1";
			this.svr__Github1.Size = new System.Drawing.Size(458, 189);
			this.svr__Github1.TabIndex = 0;
			// 
			// svr___vs__Ok
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.button1);
			this.Controls.Add(this.svr__Github1);
			this.Name = "svr___vs__Ok";
			this.Size = new System.Drawing.Size(464, 233);
			this.ResumeLayout(false);

		}

		#endregion

		private svr.client_.svr_client__Gitlab svr__Github1;
		private System.Windows.Forms.Button button1;
	}
}
