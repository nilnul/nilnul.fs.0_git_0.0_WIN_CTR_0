﻿using nilnul.fs.git.svr.client_;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nilnul.fs._git_._WIN_CTR_.svr.client
{
	/// <summary>
	/// caption of this control is the name. click the name to popup a dialog to edit the settings of a client-by-provider
	/// </summary>
	public partial class Titled : UserControl
	{
		public Titled()
		{
			InitializeComponent();
		}

		/// <summary>
		/// may be null
		/// </summary>
		private nilnul.fs.git.svr.client_.accVaulted.Named _noCred;

		public nilnul.fs.git.svr.client_.accVaulted.Named noCred
		{
			get
			{
				return _noCred;
			}
			set
			{
				_noCred = value;
			}
		}





		private void button1_Click(object sender, EventArgs e)
		{
			var f = new nilnul.fs._git_._WIN_CTR_.svr._clients.add.ok.Form1();
			f.noCred = AccVaulted.Coerce( _noCred.client );
			var r= f.ShowDialog();
			if (r== DialogResult.OK)
			{
				_noCred.clientSetter= f.noCred;
				//_noCred = f.noCred;
			}
		}
	}
}
