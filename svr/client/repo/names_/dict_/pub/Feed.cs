﻿using nilnul.fs.git.svr.client_.vaulted.repo.names_;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nilnul.fs._git_._WIN_CTR_.svr.client.repo.names_.dict_.pub
{
	public partial class svr_clients__pub_Feed : UserControl
		,
		nilnul._obj_._CTR4WIN_._feed_.BeOkI
		,
		nilnul._obj_._CTR4WIN_._feed_.BeErrI
		,
		nilnul._obj_._CTR4WIN_._feed_.ReportI
		,
		nilnul._obj_._CTR4WIN_.FeedI





	{
		public event Action report;
		public svr_clients__pub_Feed()
		{
			InitializeComponent();
		}

		public bool beOk => this.eg__xpn_report_ForInput1.beOk;

		public bool beErr => !beOk;



		

		public object val => names;

		public Dict names { get => this.svr_clients__Pub1.names; set => this.svr_clients__Pub1.names = value; }

		/// <summary>
		/// whether names are distinct
		/// </summary>
		/// <returns></returns>
		public string check(

		) {
			///partition into group
			///
			var t = this.svr_clients__Pub1.nameds.GroupBy(
				x=>x.Item1
			).Where(g=>g.Count()>1).ToArray();

			if (t.Any())
			{

				return "Repeated names:\n\r" + string.Join(
					Environment.NewLine
					,
					
					t.Select(
						x=>
						string.Join(
							","
							,
							x.Key
						)
					)
				);
			}
			
			return "";


		}

		private void svr_clients__Pub1_report()
		{
			var oldOk = this.eg__xpn_report_ForInput1.beOk;
			this.eg__xpn_report_ForInput1.xpn = check();
			if (beOk || oldOk)
			{
				report?.Invoke();
			}
		}
	}
}
