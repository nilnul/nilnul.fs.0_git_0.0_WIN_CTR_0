﻿
namespace nilnul.fs._git_._WIN_CTR_.svr.client.repo.names_.dict_.pub
{
	partial class svr_clients__pub_Feed
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(svr_clients__pub_Feed));
			this.eg__xpn_report_ForInput1 = new nilnul._obj_._CTR4WIN_.eg_.xpn.report.eg__xpn_report_ForInput();
			this.svr_clients__Pub1 = new svr_client_repo_names__dict__Pub();
			((System.ComponentModel.ISupportInitialize)(this.eg__xpn_report_ForInput1)).BeginInit();
			this.eg__xpn_report_ForInput1.Panel1.SuspendLayout();
			this.eg__xpn_report_ForInput1.SuspendLayout();
			this.SuspendLayout();
			// 
			// eg__xpn_report_ForInput1
			// 
			this.eg__xpn_report_ForInput1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.eg__xpn_report_ForInput1.Location = new System.Drawing.Point(0, 0);
			this.eg__xpn_report_ForInput1.Name = "eg__xpn_report_ForInput1";
			this.eg__xpn_report_ForInput1.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// eg__xpn_report_ForInput1.Panel1
			// 
			this.eg__xpn_report_ForInput1.Panel1.Controls.Add(this.svr_clients__Pub1);
			this.eg__xpn_report_ForInput1.Panel2Collapsed = true;
			this.eg__xpn_report_ForInput1.Size = new System.Drawing.Size(557, 463);
			this.eg__xpn_report_ForInput1.TabIndex = 0;
			this.eg__xpn_report_ForInput1.xpn = "";
			// 
			// svr_clients__Pub1
			// 
			this.svr_clients__Pub1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.svr_clients__Pub1.Location = new System.Drawing.Point(0, 0);
			this.svr_clients__Pub1.Name = "svr_clients__Pub1";
			//this.svr_clients__Pub1.nameds = ((nilnul.fs.git.svr.client_.accVaulted.Nameds)(resources.GetObject("svr_clients__Pub1.nameds")));
			this.svr_clients__Pub1.Size = new System.Drawing.Size(557, 463);
			this.svr_clients__Pub1.TabIndex = 0;
			this.svr_clients__Pub1.report += new System.Action(this.svr_clients__Pub1_report);
			// 
			// Validating
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.eg__xpn_report_ForInput1);
			this.Name = "Validating";
			this.Size = new System.Drawing.Size(557, 463);
			this.eg__xpn_report_ForInput1.Panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.eg__xpn_report_ForInput1)).EndInit();
			this.eg__xpn_report_ForInput1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private _obj_._CTR4WIN_.eg_.xpn.report.eg__xpn_report_ForInput eg__xpn_report_ForInput1;
		private svr_client_repo_names__dict__Pub svr_clients__Pub1;
	}
}
