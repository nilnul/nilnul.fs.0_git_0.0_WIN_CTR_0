﻿
namespace nilnul.fs._git_._WIN_CTR_.svr.client.repo.names_.dict_.pub.feed.ok
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.svr_clients__pub_feed_Ok1 = new nilnul.fs._git_._WIN_CTR_.svr.client.repo.names_.dict_.pub.feed.svr_clients__pub_feed_Ok();
			this.SuspendLayout();
			// 
			// svr_clients__pub_feed_Ok1
			// 
			resources.ApplyResources(this.svr_clients__pub_feed_Ok1, "svr_clients__pub_feed_Ok1");
			this.svr_clients__pub_feed_Ok1.Name = "svr_clients__pub_feed_Ok1";
			this.svr_clients__pub_feed_Ok1.names = ((nilnul.fs.git.svr.client_.vaulted.repo.names_.Dict)(resources.GetObject("svr_clients__pub_feed_Ok1.names")));
			this.svr_clients__pub_feed_Ok1.ok += new System.Action(this.svr_clients__pub_feed_Ok1_ok);
			// 
			// Form1
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.svr_clients__pub_feed_Ok1);
			this.Name = "Form1";
			this.ShowInTaskbar = false;
			this.ResumeLayout(false);

		}

		#endregion

		private svr_clients__pub_feed_Ok svr_clients__pub_feed_Ok1;
	}
}