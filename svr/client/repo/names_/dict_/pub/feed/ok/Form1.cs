﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nilnul.fs._git_._WIN_CTR_.svr.client.repo.names_.dict_.pub.feed.ok
{
	public partial class Form1
		:
		Form
		//nilnul._obj_._CTR4WIN_.feed.ok.Form1
	{
		public Form1()
		{
			InitializeComponent();
		}

		public nilnul.fs.git.svr.client_.vaulted.repo.names_.Dict val
		{
			get
			{
				return this.svr_clients__pub_feed_Ok1.names;
			}
			set
			{

				this.svr_clients__pub_feed_Ok1.names = value;
			}
		}


		private void svr_clients__pub_feed_Ok1_ok()
		{
			this.DialogResult = DialogResult.OK;
			this.Close();

		}
	}
}
