﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nilnul.fs._git_._WIN_CTR_.svr.client.repo.names_.dict_.pub.feed
{
	public partial class svr_clients__pub_feed_Ok : UserControl
	{
		public event Action ok;
		public svr_clients__pub_feed_Ok()
		{
			InitializeComponent();
		}

		public nilnul.fs.git.svr.client_.vaulted.repo.names_.Dict names
		{
			get
			{
				return this.svr_clients__pub_Feed1.names;
			}
			set
			{

				this.svr_clients__pub_Feed1.names = value;
			}
		}

		private void feed_Ok1_oked()
		{
			///save.


			//nilnul.fs.git.Properties.Settings.Default.clients_pub = this.svr_clients__pub_Feed1.nameds;


			nilnul.fs.git.Properties.SettingsX.clients_pub_dict= this.svr_clients__pub_Feed1.names;


			nilnul.fs.git.Properties.Settings.Default.Save();

			#region ///////don't do the following as upgrade will bring out pre-revised data.
			//#if false
			//nilnul.fs.git.Properties.Settings.Default.Upgrade();
			//	nilnul.fs.git.Properties.Settings.Default.Reload();
			//#endif
			#endregion
			ok?.Invoke();
		}
	}
}