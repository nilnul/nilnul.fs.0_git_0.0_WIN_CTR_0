﻿namespace nilnul.fs._git_._WIN_CTR_.svr.client.titleds.ok
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.clients__Ok1 = new nilnul.fs._git_._WIN_CTR_.svr.client.titleds.svr_client_titleds_Ok();
            this.SuspendLayout();
            // 
            // clients__Ok1
            // 
            this.clients__Ok1.clients = ((nilnul.fs.git.svr.client_.accVaulted.Nameds)(resources.GetObject("clients__Ok1.clients")));
            this.clients__Ok1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clients__Ok1.Location = new System.Drawing.Point(0, 0);
            this.clients__Ok1.Name = "clients__Ok1";
            this.clients__Ok1.Size = new System.Drawing.Size(800, 415);
            this.clients__Ok1.TabIndex = 0;
            this.clients__Ok1.oked += new System.Action(this.clients__Ok1_oked);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 415);
            this.Controls.Add(this.clients__Ok1);
            this.Name = "Form1";
            this.ShowInTaskbar = false;
            this.Text = "Clients";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

		}

		#endregion

		private svr_client_titleds_Ok clients__Ok1;
	}
}