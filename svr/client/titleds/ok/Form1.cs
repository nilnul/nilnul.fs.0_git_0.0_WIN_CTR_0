﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nilnul.fs._git_._WIN_CTR_.svr.client.titleds.ok
{
	public partial class Form1 : Form
	{
		public event Action oked;

		public Form1()
		{
			InitializeComponent();
		}



		public nilnul.fs.git.svr.client_.accVaulted.Nameds clients
		{
			get
			{
				return this.clients__Ok1.clients;
			}
			set
			{
				this.clients__Ok1.clients = value;
			}
		}

		public void loadCfg()
		{
			clients__Ok1.loadCfg();
		}


		private void clients__Ok1_oked()
		{
			this.DialogResult = DialogResult.OK;
			this.Close();
			oked?.Invoke();

		}

		private void Form1_Load(object sender, EventArgs e)
		{
			loadCfg();
		}
	}
}
