﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nilnul.fs._git_._WIN_CTR_.svr.client.titleds
{
	public partial class svr_client_titleds_Ok : UserControl
	{
		public event Action oked;
		public svr_client_titleds_Ok()
		{
			InitializeComponent();
		}


		/// <summary>
		/// save the client.NoCreds
		/// </summary>
		private void Ok2_oked()
		{


			//nilnul.fs.git.Properties.Settings.Default.client__accVaulted_Nameds = this.clients1.nameds;
			nilnul.fs.git.Properties.SettingsX.Clients_my= this.clients1.nameds;


			nilnul.fs.git.Properties.Settings.Default.Save();
			#region ///////don't do the following as upgrade will bring out pre-revised data.
//#if false
			//nilnul.fs.git.Properties.Settings.Default.Upgrade();
		//	nilnul.fs.git.Properties.Settings.Default.Reload();


//#endif

			#endregion

			oked?.Invoke();
		}

		public void loadCfg()
		{
			clients1.loadCfg();
		}


		public nilnul.fs.git.svr.client_.accVaulted.Nameds clients
		{
			get
			{
				return this.clients1.nameds;
			}
			set
			{
				this.clients1.nameds = value;
			}
		}


	}
}
