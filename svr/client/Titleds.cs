﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nilnul.fs._git_._WIN_CTR_.svr.client
{
	/// <summary>
	/// name is edited in place.
	/// when name is double clicked, editor is opened and editing is enabled. in the editor , name is not included.
	/// when names conflict, an error is displayed in validator; but not in an only single name. the validator is not in this control; rather, we will extend this control in a new aggregate control. Or else, we can  append a ver/hash in same names.
	/// </summary>
	public partial class Titleds : UserControl
	{
		public Titleds()
		{
			InitializeComponent();
			loadCfg();
		}
		public void loadCfg()
		{

			this.nameds = nilnul.fs.git.Properties.SettingsX.Clients_my//.client__noCred_Nameds;// ..Settings.Default.client_NoCreds
						//??new fs.git.client.NoCreds()
			;
		}

		public nilnul.fs.git.svr.client_.accVaulted.Nameds nameds
		{
			get
			{
				return new git.svr.client_.accVaulted.Nameds(
					this.listBox1.Items.Cast<ListViewItem>().Select(item => new git.svr.client_.accVaulted.Named(item.Text, (git.svr.client_.AccVaultedI)item.Tag))
				);
			}
			set
			{
				this.listBox1.Items.Clear();
				foreach (var item in value)
				{
					var listViewItem = new ListViewItem(item.name);
					setTag(ref listViewItem, item.client);
					//listViewItem.Tag = item.data.client;
					this.listBox1.Items.Add(listViewItem);
				}
			}
		}


		void setTag(ref ListViewItem item, fs.git.svr.client_.AccVaultedI noCred)
		{
			item.Tag = noCred;
		}


		fs.git.svr.client_.AccVaultedI getTag(ListViewItem item)
		{

			return item.Tag as fs.git.svr.client_.AccVaultedI;
		}


		private void Clients_Load(object sender, EventArgs e)
		{
			//loadCfg();
		}

		private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
		{
			//this.listBox1.Items[]

		}

		/// <summary>
		/// when user clicks one existant item to open it
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void listBox1_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			//this.listBox1.SelectedItem;
			var index = this.listBox1.SelectedIndices;
			if (index.Count != 1)
			{
				return;
			}

			var indexOnly = index[0];

			var client = getTag(this.listBox1.Items[indexOnly]); //may be null; null means a new box cancelled

			switch (client)
			{
				case null:
					break;
				case nilnul.fs.git.svr_.amazon.client_.AccInVault amazon:
					var lowrContent = new svr.client_.amazon.scrollable.ok.Form1() { };
					lowrContent.accVaulted = amazon;
					var dr = lowrContent.ShowDialog();
					if (dr == DialogResult.OK)
					{
						this.listBox1.Items[indexOnly].Tag = lowrContent.accVaulted;
					}
					break;

				case nilnul.fs.git.svr_.github.client_.Vaulted github1:

					var githubDialog1 = new svr_.github.client.scrollable.ok.Form1() { };

					githubDialog1.accVaulted = github1;

					var dResult4github1 = githubDialog1.ShowDialog();
					if (dResult4github1 == DialogResult.OK)
					{
						this.listBox1.Items[indexOnly].Tag = githubDialog1.accVaulted;
					}
					break;
				case nilnul.fs.git.svr.client_.accVaulted_.Github github:

					var githubDialog = new svr.client_.github.scrollable.ok.Form1() { };

					githubDialog.accVaulted = github;

					var dResult = githubDialog.ShowDialog();
					if (dResult == DialogResult.OK)
					{
						this.listBox1.Items[indexOnly].Tag = githubDialog.accVaulted;
					}
					break;

				case nilnul.fs.git.svr.client_.accVaulted_.GitlabI gitlab:

					var gitlabDialog = new svr.client_.gitlab.scrollable.ok.Form1() { };

					gitlabDialog.accVaulted =  new git.svr_.gitlab.client_.AccInVault(gitlab.usr); ;

					var dResult1 = gitlabDialog.ShowDialog();
					if (dResult1 == DialogResult.OK)
					{
						this.listBox1.Items[indexOnly].Tag = gitlabDialog.accVaulted;
					}


					break;

				case nilnul.fs.git.svr_.vs.client_.AccInVault vs:

					var vsDialog = new svr.client_.vs.scrollable.ok.Form1() { };


					vsDialog.accVaulted = vs;

					var dResult11 = vsDialog.ShowDialog();
					if (dResult11 == DialogResult.OK)
					{
						this.listBox1.Items[indexOnly].Tag = vsDialog.accVaulted;
					}

					break;

				case nilnul.fs.git.svr_.azure.client_.Vaulted vs11:

					var vsDialog11 = new svr_.azure.client.scrollable.ok.Form1() { };


					vsDialog11.accVaulted = vs11;

					var dResult111 = vsDialog11.ShowDialog();
					if (dResult111 == DialogResult.OK)
					{
						this.listBox1.Items[indexOnly].Tag = vsDialog11.accVaulted;
					}

					break;

				default:
					throw new UnexpectedTypeException(
						client.ToString()
					);
					//throw new NotImplementedException();

			}




		}
		public void add(nilnul.fs.git.svr.client_.accVaulted.Named client)
		{
			var listViewItem = new ListViewItem(client.name);
			listViewItem.Tag = client.client;
			this.listBox1.Items.Add(listViewItem);


			// Place the newly-added item into edit mode immediately
			listViewItem.BeginEdit();
		}
		/// <summary>
		/// create, then add
		/// </summary>
		void createAndAdd()
		{

			var dialog = new _clients.add.ok.Form1();

			var r = dialog.ShowDialog();

			if (r == DialogResult.OK)       ///edited and confirmed
			{
				var listitem = new ListViewItem(
					dialog.svr
				);
				setTag(ref listitem, dialog.noCred);

				var added = this.listBox1.Items.Add(listitem);

			}


		}
		void addNameFirst()
		{
			///add one with null as the backend unnamed-client
			var listViewItem = new ListViewItem("client1");
			this.listBox1.Items.Add(listViewItem);

			// Place the newly-added item into edit mode immediately
			listViewItem.BeginEdit();

		}

		private void addToolStripMenuItem_Click(object sender, EventArgs e)
		{
			createAndAdd();

		}

		private void removeToolStripMenuItem_Click(object sender, EventArgs e)
		{

			//ListViewHitTestInfo lstHitTestInfo = listBox1.HitTest(e..X, e.Y);
			//                if (e.Button == MouseButtons.Right)
			//                {
			//                    if (lstHitTestInfo.Item != null)
			//                    {
			//                        listView1.ContextMenuStrip = contextMenuStrip1;
			//                    }
			//                }
			if (listBox1.SelectedItems.Count != 0)
			{
				foreach (ListViewItem LItem in listBox1.SelectedItems)
				{
					//Your code

					listBox1.Items.Remove(LItem);


				}
			}
		}

		private void contextMenuStrip2client_Opening(object sender, CancelEventArgs e)
		{

		}
	}
}
