﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using nilnul.fs.git.svr.client_.accVaulted_;
using nilnul.fs.git.svr_.amazon.client_;

namespace nilnul.fs._git_._WIN_CTR_.svr._clients
{
	/// <summary>
	/// when one update a client, the selection of different servers shall not be provided. So, "Update" panel shall be individually coded for each svr.
	/// </summary>
	public partial class svr__clients_Upd : UserControl
	{
		public svr__clients_Upd()
		{
			InitializeComponent();
			this.comboBox1.DataSource = nilnul.fs.git._svrs.EnumX.Names();
		}

		void _valide()
		{


		}

		public string svr
		{
			get
			{
				return this.comboBox1.Text;
			}
			set {
				this.comboBox1.Text = value;
			}
		}




		public nilnul.fs.git.svr.client_.AccVaultedI accVaulted
		{
			get
			{
				switch (this.comboBox1.Text)
				{
					case nameof(nilnul.fs.git._svrs._EnumX.Github) :
						return /*new git.svr.client_.AccVaulted*/(
							((svr.client_.svr_client__Github)this.splitContainer1.Panel2.Controls[0]).accVaulted

						);

					case nameof(nilnul.fs.git._svrs._EnumX.Gitlab):
						return /*new git.svr.client_.NoCred*/(
							(
								(svr.client_.svr_client__Gitlab)this.splitContainer1.Panel2.Controls[0]
							).accVaulted
						);
					
					case nameof(nilnul.fs.git._svrs._EnumX.VisualStudio) :
						return /*new git.svr.client_.NoCred*/(
							(
								(svr.client_.svr_client__Vs)this.splitContainer1.Panel2.Controls[0]
							).accVaulted
						);
					case nameof(nilnul.fs.git._svrs.EnumX.Amazon):
						return /*new git.svr.client_.NoCred*/(
							(
								(svr.client_.svr_client__Amazon)this.splitContainer1.Panel2.Controls[0]
							).accVaulted
						);

						break;


					default:
						throw new UnexpectedReachException();
						break;
				}
			}
			set
			{
				switch (value)
				{
					case nilnul.fs.git.svr.client_.accVaulted_.Github github:
						this.comboBox1.Text = nameof(nilnul.fs.git._svrs._EnumX.Github);
						lowerLoadGithub((github));
						break;
					case nilnul.fs.git.svr.client_.accVaulted_.GitlabI gitlab:
						this.comboBox1.Text = nameof(nilnul.fs.git._svrs._EnumX.Gitlab);
						lowerLoad(gitlab);
						break;
					case nilnul.fs.git.svr_.amazon.client_.AccInVault amazon:
						this.comboBox1.Text = nameof(nilnul.fs.git._svrs._EnumX.Amazon);

						lowerLoad(amazon);
						break;
					case nilnul.fs.git.svr_.vs.client_.AccInVault vs:
						this.comboBox1.Text = nameof(nilnul.fs.git._svrs._EnumX.VisualStudio);

						lowerLoad(vs);
						break;

					default:
						throw new NotImplementedException();
				}
			}
		}


		private void lowerLoad(GitlabI gitlab)
		{

			var lowrContent = new svr.client_.svr_client__Gitlab() { Dock = DockStyle.Fill };
			lowrContent.accVaulted = new git.svr_.gitlab.client_.AccInVault(gitlab.usr);
			//lowrContent.accVaulted = ( gitlab );

			this.splitContainer1.Panel2.Controls.Clear();

			this.splitContainer1.Panel2.Controls.Add(
				lowrContent
			);

		}


		private void lowerLoadGithub(Github github)
		{

			var lowrContent = new svr.client_.svr_client__Github() { Dock = DockStyle.Fill };
			lowrContent.accVaulted = new git.svr.client_.accVaulted_.Github(github.header);

			this.splitContainer1.Panel2.Controls.Clear();

			this.splitContainer1.Panel2.Controls.Add(
				lowrContent
			);


		}

		private void lowerLoad(AccInVault amazon)
		{
			var lowrContent = new svr.client_.svr_client__Amazon() { Dock = DockStyle.Fill };
			lowrContent.accVaulted = amazon;

			this.splitContainer1.Panel2.Controls.Clear();

			this.splitContainer1.Panel2.Controls.Add(
				lowrContent
			);


		}

		private void lowerLoad(git.svr_.vs.client_.AccInVault vs)
		{
			var lowrContent = new svr.client_.svr_client__Vs() { Dock = DockStyle.Fill };
			lowrContent.accVaulted = vs;

			this.splitContainer1.Panel2.Controls.Clear();

			this.splitContainer1.Panel2.Controls.Add(
				lowrContent
			);
		}


		private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
		{
			switch (this.comboBox1.Text)
			{
				case nameof(nilnul.fs.git._svrs._EnumX.VisualStudio):
					this.splitContainer1.Panel2.Controls.Clear();
					this.splitContainer1.Panel2.Controls.Add(
						new svr.client_.svr_client__Vs() { Dock = DockStyle.Fill }
					);
					break;
				case nameof(nilnul.fs.git._svrs._EnumX.Github) :
					lowerLoadGithub();
					break;
				case nameof(nilnul.fs.git._svrs._EnumX.Gitlab) :
					lowerNewGitlab();
					break;
				case nameof( nilnul.fs.git._svrs._EnumX.Amazon ):

					lowerLoadAmazon();
					break;
				default:
					lowerLoadUnknown();

					break;
			}
		}

		private void lowerLoadGithub()
		{
			var lowrContent = new svr.client_.svr_client__Github() { Dock = DockStyle.Fill };
			lowrContent.accVaulted = new git.svr.client_.accVaulted_.Github();

			this.splitContainer1.Panel2.Controls.Clear();

			this.splitContainer1.Panel2.Controls.Add(
				lowrContent
			);

		}

		private void lowerLoadAmazon()
		{
			var lowrContent = new svr.client_.svr_client__Amazon() { Dock = DockStyle.Fill };
			lowrContent.accVaulted = new git.svr_.amazon.client_.AccInVault();

			this.splitContainer1.Panel2.Controls.Clear();

			this.splitContainer1.Panel2.Controls.Add(
				lowrContent
			);

		}

		private void lowerLoadUnknown()
		{
			var lowrContent = new svr._clients._add.UnknownClient() { Dock = DockStyle.Fill };

			this.splitContainer1.Panel2.Controls.Clear();

			this.splitContainer1.Panel2.Controls.Add(
				lowrContent
			);

		}



		private void lowerNewGitlab()
		{

			var lowrContent = new svr.client_.svr_client__Gitlab() { Dock = DockStyle.Fill };
			lowrContent.accVaulted = new git.svr_.gitlab.client_.AccInVault();

			this.splitContainer1.Panel2.Controls.Clear();

			this.splitContainer1.Panel2.Controls.Add(
				lowrContent
			);

		}




		//public void lowerLoadGithub(git.svr_.github.Client github)
		//{
		//	var lowrContent = new svr.client_.svr_client__Github() { Dock = DockStyle.Fill };
		//	lowrContent.accVaulted = new git.svr.client_.accVaulted_.Github(github.header);

		//	this.splitContainer1.Panel2.Controls.Clear();

		//	this.splitContainer1.Panel2.Controls.Add(
		//		lowrContent
		//	);

		//}

		//public void load(nilnul.fs.git.svr.ClientA client)
		//{
		//	if (client is nilnul.fs.git.svr_.github.Client github)
		//	{
		//		this.comboBox1.Text = "github";

		//		lowerLoadGithub(
		//			github
		//		);

		//	}
		//}


	}
}
