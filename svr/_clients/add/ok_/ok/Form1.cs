﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nilnul.fs._git_._WIN_CTR_._client_._ok_
{
	[Obsolete( nameof(svr._clients.add.ok.Form1))]
	public partial class Form1 : Form
	{
		public event Action oked;

		public event Action<nilnul.fs.git.client.NoCred> oked1;
		public Form1()
		{
			InitializeComponent();
		}

		private void svr___github__Ok1_Load(object sender, EventArgs e)
		{
			
		}

		public nilnul.fs.git.client.NoCred noCred {
			get {
				return this.svr___github__Ok1.noCred;
			}
			//set
			//{
			//	this.svr___github__Ok1.val = value; ;
			//}
		}

		private void Svr___vs__Ok1_oked()
		{
			this.Close();
			oked?.Invoke();
			oked1?.Invoke(this.noCred);
		}
	}
}
