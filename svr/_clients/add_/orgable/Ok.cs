﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nilnul.fs._git_._WIN_CTR_.svr._clients.add_.orgable
{
	public partial class svr__clients_add_Ok : UserControl
	{
		public event Action oked;
		public svr__clients_add_Ok()
		{
			InitializeComponent();
		}

		public nilnul.fs.git.svr.client_.AccVaultedI noCred {
			get {
				return this.svr1.accVaulted;
			}
			set
			{
				this.svr1.accVaulted = value;
			}
		}

		public string svr {
			get {
				return this.svr1.svr;
			}
			set {
				this.svr1.svr = value;
			}
		}

		private void Ok2_oked()
		{

			//todo: save acc
			this.svr1.saveCred();

			oked?.Invoke();
		}

		private void Ok2_Panel2_Paint(object sender, PaintEventArgs e)
		{

		}
	}
}
