﻿namespace nilnul.fs._git_._WIN_CTR_.svr._clients.add_.orgable
{
	partial class svr__clients_add_Ok
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.ok2 = new _nilnul_._CTR4WIN_.Ok();
			this.svr1 = new svr__clients_add__GithubOrgable();
			((System.ComponentModel.ISupportInitialize)(this.ok2)).BeginInit();
			this.ok2.Panel1.SuspendLayout();
			this.ok2.SuspendLayout();
			this.SuspendLayout();
			// 
			// ok2
			// 
			this.ok2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ok2.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
			this.ok2.Location = new System.Drawing.Point(0, 0);
			this.ok2.Name = "ok2";
			this.ok2.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// ok2.Panel1
			// 
			this.ok2.Panel1.Controls.Add(this.svr1);
			// 
			// ok2.Panel2
			// 
			this.ok2.Panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.Ok2_Panel2_Paint);
			this.ok2.Size = new System.Drawing.Size(687, 509);
			this.ok2.SplitterDistance = 459;
			this.ok2.TabIndex = 0;
			this.ok2.oked += new System.Action(this.Ok2_oked);
			// 
			// svr1
			// 
			this.svr1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.svr1.Location = new System.Drawing.Point(0, 0);
			this.svr1.Name = "svr1";
			this.svr1.Size = new System.Drawing.Size(687, 459);
			this.svr1.TabIndex = 0;
			// 
			// Ok
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.ok2);
			this.Name = "Ok";
			this.Size = new System.Drawing.Size(687, 509);
			this.ok2.Panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.ok2)).EndInit();
			this.ok2.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private _nilnul_._CTR4WIN_.Ok ok2;
		private svr__clients_add__GithubOrgable svr1;
	}
}
