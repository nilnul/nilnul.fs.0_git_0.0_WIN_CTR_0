﻿namespace nilnul.fs._git_._WIN_CTR_.svr._clients.add_.orgable.ok
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.svr__clients_add_Ok1 = new nilnul.fs._git_._WIN_CTR_.svr._clients.add_.orgable.svr__clients_add_Ok();
			this.SuspendLayout();
			// 
			// svr__clients_add_Ok1
			// 
			resources.ApplyResources(this.svr__clients_add_Ok1, "svr__clients_add_Ok1");
			this.svr__clients_add_Ok1.Name = "svr__clients_add_Ok1";
			this.svr__clients_add_Ok1.noCred = ((nilnul.fs.git.svr.client_.AccVaultedI)(resources.GetObject("svr__clients_add_Ok1.noCred")));
			this.svr__clients_add_Ok1.svr = "Amazon";
			this.svr__clients_add_Ok1.oked += new System.Action(this.Svr___vs__Ok1_oked);
			this.svr__clients_add_Ok1.Load += new System.EventHandler(this.svr_____Ok1_Load);
			// 
			// Form1
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.svr__clients_add_Ok1);
			this.Name = "Form1";
			this.ShowInTaskbar = false;
			this.ResumeLayout(false);

		}

		#endregion

		private _clients.add_.orgable.svr__clients_add_Ok svr__clients_add_Ok1;
	}
}