﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nilnul.fs._git_._WIN_CTR_.svr._clients.add_.orgable.ok
{
	public partial class Form1 : Form
	{
		public event Action oked;

		public event Action<nilnul.fs.git.svr.client_.AccVaultedI> oked1;
		public Form1()
		{
			InitializeComponent();
		}

		private void svr_____Ok1_Load(object sender, EventArgs e)
		{
			
		}

		public nilnul.fs.git.svr.client_.AccVaultedI noCred {
			get {
				return this.svr__clients_add_Ok1.noCred;
			}
			set
			{
				this.svr__clients_add_Ok1.noCred = value; ;
			}
		}

		public string svr
		{
			get
			{
				return this.svr__clients_add_Ok1.svr;
			}
			set {
				this.svr__clients_add_Ok1.svr = value;
			}
		}
		private void Svr___vs__Ok1_oked()
		{
			this.DialogResult = DialogResult.OK;
			this.Close();
			oked?.Invoke();
			oked1?.Invoke(this.noCred);
		}
	}
}
