﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nilnul.fs._git_._WIN_CTR_.svr
{
	/// <summary>
	/// name is edited in place.
	/// when name is double clicked, editor is opened and editing is enabled. in the editor , name is not included.
	/// when names conflict, an error is displayed in validator; but not in an only single name. the validator is not in this control; rather, we will extend this control in a new aggregate control. Or else, we can  append a ver/hash in same names.
	/// </summary>
	public partial class svr_Clients : UserControl
	{
		public svr_Clients()
		{
			InitializeComponent();
		}
		void loadCfg()
		{
			this.noCreds = nilnul.fs.git.Properties.SettingsX.Clients_my;//.client__noCred_Nameds;// ..Settings.Default.client_NoCreds??new git.client.NoCreds();

		}
		public nilnul.fs.git.svr.client_.accVaulted.Nameds noCreds
		{
			get
			{
				return new git.svr.client_.accVaulted.Nameds(
					this.listBox1.Items.Cast<git.svr.client_.accVaulted.Named>()
				);
			}
			set
			{
				this.listBox1.Items.Clear();
				foreach (var item in value)
				{
					this.listBox1.Items.Add(item);
				}
			}
		}

		public void add( nilnul.fs.git.svr.client_.accVaulted.Named client) {
			this.listBox1.Items.Add(
				client
				);
		}

		private void Clients_Load(object sender, EventArgs e)
		{
			loadCfg();
		}

		private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
		{
			//this.listBox1.Items[]

		}

		private void listBox1_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			//this.listBox1.SelectedItem;
			int index = this.listBox1.IndexFromPoint(e.Location);
			if (index != System.Windows.Forms.ListBox.NoMatches)
			{
				var named= this.listBox1.Items[index] as git.svr.client_.accVaulted.Named;

				var dialog = new _clients.add.ok.Form1();

				dialog.noCred = new git.svr.client_.AccVaulted( named.client );

				var r=dialog.ShowDialog();

				if (r == DialogResult.OK)		///edited and confirmed
				{
					this.listBox1.Items[index] = dialog.noCred;
				}

			}
		}
	}
}
