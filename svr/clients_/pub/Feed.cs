﻿using nilnul.fs.git.svr.client_.vaulted.repo.names_;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nilnul.fs._git_._WIN_CTR_.svr.clients_.pub
{
	[Obsolete(nameof(svr.client.repo.names_.dict_.svr_client_repo_names__dict__Pub), true)]
	public partial class svr_clients__pub_Feed : UserControl
		,
		nilnul._obj_._CTR4WIN_._feed_.BeOkI
		,
		nilnul._obj_._CTR4WIN_._feed_.BeErrI
		,
		nilnul._obj_._CTR4WIN_._feed_.ReportI
		,
		nilnul._obj_._CTR4WIN_.FeedI





	{
		public event Action report;
		public svr_clients__pub_Feed()
		{
			InitializeComponent();
		}

		public bool beOk => this.eg__xpn_report_ForInput1.beOk;

		public bool beErr => !beOk;


		[Obsolete("",true)]
		public nilnul.fs.git.svr.client.repo.sortie_.byHost.biject_.Nom1nonemptyEs nameds
		{
			get
			{
				return this.svr_clients__Pub1.nameds;
			}
			set
			{

				this.svr_clients__Pub1.nameds=value;
			}
		}

		public object val => names;

		public Dict names { get => this.svr_clients__Pub1.names; set => this.svr_clients__Pub1.names = value; }

		/// <summary>
		/// whether names are distinct
		/// </summary>
		/// <returns></returns>
		public string check(

		) {





			var names = this.svr_clients__Pub1.nameds.Select(
				n =>
				string.Join(
					"-"
					,
					n.nom
				)

			);
			return nilnul.obj.sortie.op_.of_._str.Check<string>.Singleton.check(names);


		}

		private void svr_clients__Pub1_report()
		{
			var oldOk = this.eg__xpn_report_ForInput1.beOk;
			this.eg__xpn_report_ForInput1.xpn = check();
			if (beOk || oldOk)
			{
				report?.Invoke();
			}
		}
	}
}
