﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nilnul.fs._git_._WIN_CTR_.svr.clients_.pub.feed
{

	[Obsolete(nameof(svr.client.repo.names_.dict_.svr_client_repo_names__dict__Pub), true)]
	public partial class svr_clients__pub_feed_Ok : UserControl
	{
		public event Action ok;
		public svr_clients__pub_feed_Ok()
		{
			InitializeComponent();
		}

		[Obsolete("",false)]
		public nilnul.fs.git.svr.client.repo.sortie_.byHost.biject_.Nom1nonemptyEs nameds
		{
			get
			{
				return this.svr_clients__pub_Feed1.nameds;
			}
			set
			{

				this.svr_clients__pub_Feed1.nameds = value;
			}
		}

		private void feed_Ok1_oked()
		{
			///save.


			//nilnul.fs.git.Properties.Settings.Default.clients_pub = this.svr_clients__pub_Feed1.nameds;


			nilnul.fs.git.Properties.SettingsX.clients_pub_dict= this.svr_clients__pub_Feed1.names;


			nilnul.fs.git.Properties.Settings.Default.Save();

			#region ///////don't do the following as upgrade will bring out pre-revised data.
			//#if false
			//nilnul.fs.git.Properties.Settings.Default.Upgrade();
			//	nilnul.fs.git.Properties.Settings.Default.Reload();
			//#endif
			#endregion
			ok?.Invoke();
		}
	}
}