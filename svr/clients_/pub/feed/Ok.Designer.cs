﻿
namespace nilnul.fs._git_._WIN_CTR_.svr.clients_.pub.feed
{
	partial class svr_clients__pub_feed_Ok
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(svr_clients__pub_feed_Ok));
			this.feed_Ok1 = new nilnul._obj_._CTR4WIN_.feed.feed_Ok();
			this.svr_clients__pub_Feed1 = new nilnul.fs._git_._WIN_CTR_.svr.clients_.pub.svr_clients__pub_Feed();
			((System.ComponentModel.ISupportInitialize)(this.feed_Ok1)).BeginInit();
			this.feed_Ok1.Panel1.SuspendLayout();
			this.feed_Ok1.SuspendLayout();
			this.SuspendLayout();
			// 
			// feed_Ok1
			// 
			this.feed_Ok1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.feed_Ok1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
			this.feed_Ok1.Location = new System.Drawing.Point(0, 0);
			this.feed_Ok1.Name = "feed_Ok1";
			this.feed_Ok1.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// feed_Ok1.Panel1
			// 
			this.feed_Ok1.Panel1.Controls.Add(this.svr_clients__pub_Feed1);
			this.feed_Ok1.Size = new System.Drawing.Size(578, 399);
			this.feed_Ok1.SplitterDistance = 349;
			this.feed_Ok1.TabIndex = 0;
			this.feed_Ok1.oked += new System.Action(this.feed_Ok1_oked);
			// 
			// svr_clients__pub_Feed1
			// 
			this.svr_clients__pub_Feed1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.svr_clients__pub_Feed1.Location = new System.Drawing.Point(0, 0);
			this.svr_clients__pub_Feed1.Name = "svr_clients__pub_Feed1";
			//this.svr_clients__pub_Feed1.nameds = ((nilnul.fs.git.svr.client_.accVaulted.Nameds)(resources.GetObject("svr_clients__pub_Feed1.nameds")));
			this.svr_clients__pub_Feed1.Size = new System.Drawing.Size(578, 349);
			this.svr_clients__pub_Feed1.TabIndex = 0;
			// 
			// svr_clients__pub_feed_Ok
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.feed_Ok1);
			this.Name = "svr_clients__pub_feed_Ok";
			this.Size = new System.Drawing.Size(578, 399);
			this.feed_Ok1.Panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.feed_Ok1)).EndInit();
			this.feed_Ok1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private _obj_._CTR4WIN_.feed.feed_Ok feed_Ok1;
		private svr_clients__pub_Feed svr_clients__pub_Feed1;
	}
}
