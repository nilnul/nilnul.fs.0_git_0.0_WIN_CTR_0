﻿namespace nilnul.fs._git_._WIN_CTR_._clients_
{
	partial class clients__Ok
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(clients__Ok));
			this.ok2 = new _nilnul_._CTR4WIN_.Ok();
			this.clients1 = new nilnul.fs._git_._WIN_CTR_.Clients();
			((System.ComponentModel.ISupportInitialize)(this.ok2)).BeginInit();
			this.ok2.Panel1.SuspendLayout();
			this.ok2.SuspendLayout();
			this.SuspendLayout();
			// 
			// ok2
			// 
			this.ok2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ok2.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
			this.ok2.Location = new System.Drawing.Point(0, 0);
			this.ok2.Name = "ok2";
			this.ok2.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// ok2.Panel1
			// 
			this.ok2.Panel1.Controls.Add(this.clients1);
			// 
			// ok2.Panel2
			// 
			this.ok2.Panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.Ok2_Panel2_Paint);
			this.ok2.Size = new System.Drawing.Size(439, 408);
			this.ok2.SplitterDistance = 358;
			this.ok2.TabIndex = 0;
			this.ok2.oked += new System.Action(this.Ok2_oked);
			// 
			// clients1
			// 
			this.clients1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.clients1.Location = new System.Drawing.Point(0, 0);
			this.clients1.Name = "clients1";
			this.clients1.noCreds = ((nilnul.fs.git.svr.client_.AccVaultedS)(resources.GetObject("clients1.noCreds")));
			this.clients1.Size = new System.Drawing.Size(439, 358);
			this.clients1.TabIndex = 0;
			// 
			// clients__Ok
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.ok2);
			this.Name = "clients__Ok";
			this.Size = new System.Drawing.Size(439, 408);
			this.ok2.Panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.ok2)).EndInit();
			this.ok2.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private _nilnul_._CTR4WIN_.Ok ok2;
		private Clients clients1;
	}
}
