﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nilnul.fs._git_._WIN_CTR_.svr.clients
{
	public partial class Add : UserControl
	{
		public Add()
		{
			InitializeComponent();
		}

		private void button1_Click(object sender, EventArgs e)
		{

		
			var f = new _clients.add.ok.Form1();

			//f.ParentForm = this.ParentForm;

			f.oked1 += F_oked1;

			f.ShowDialog(this.ParentForm);
		}

		private void F_oked1(git.svr.client_.AccVaultedI obj)	//name will be changed separately: rename the item in place
		{
			this.svr_Clients1.add(
				new git.svr.client_.accVaulted.Named("",obj)	//if the name is empty, we then automatically let the user change the name in place hence
			);

		}

		void loadCfg()
		{
			this.noCreds = nilnul.fs.git.Properties.SettingsX.client__noCred_Nameds;// .client__NoCreds1;// ..Settings.Default.client_NoCreds??new git.client.NoCreds();
		}

		public nilnul.fs.git.svr.client_.accVaulted.Nameds noCreds
		{
			get
			{
				return 
					this.svr_Clients1.noCreds
				;
			}
			set
			{
				this.svr_Clients1.noCreds = value;
			
			}
		}
	}
}
