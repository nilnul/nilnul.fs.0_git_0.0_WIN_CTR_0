﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using nilnul.obj.str;

namespace nilnul.fs._git_._WIN_CTR_.svr.clients_
{
	/// <summary>
	/// name is edited in place.
	/// when name is double clicked, editor is opened and editing is enabled. in the editor , name is not included.
	/// when names conflict, an error is displayed in validator; but not in an only single name. the validator is not in this control; rather, we will extend this control in a new aggregate control. Or else, we can  append a ver/hash in same names.
	/// </summary>
	///
	[Obsolete(nameof(svr.client.repo.names_.dict_.svr_client_repo_names__dict__Pub))]
	public partial class svr_clients__Pub : UserControl
	{
		public event Action report;
		public svr_clients__Pub()
		{
			InitializeComponent();
			_cfgIni();
		}
		void _cfgIni()
		{
			//_namedsSet(nilnul.fs.git.Properties.SettingsX.clients_pub);
			_namesIni(nilnul.fs.git.Properties.SettingsX.clients_pub_dict);

		}

		/// <summary>
		/// in comparision with <see cref="_cfgIni"/>, an event is fired.
		/// </summary>
		public void loadCfg()
		{

			this.names = nilnul.fs.git.Properties.SettingsX.clients_pub_dict;// ..Settings.Default.client_NoCreds??new git.client.NoCreds();
																			 //this.nameds = nilnul.fs.git.Properties.SettingsX.clients_pub;// ..Settings.Default.client_NoCreds??new git.client.NoCreds();

		}
		[Obsolete(nameof(_namesIni))]
		void _namedsSet(nilnul.fs.git.svr.client.repo.sortie_.byHost.biject_.Nom1nonemptyEs value)
		{

			this.listBox1.Items.Clear();
			if (value is null)
			{
				return;
			}
			foreach (var item in value)
			{
				var listViewItem = new ListViewItem(
					string.Join(
						"-"
						,
						item.Item2
					)
				);
				setTag(ref listViewItem, item.Item1);
				//listViewItem.Tag = item.data.client;
				this.listBox1.Items.Add(listViewItem);
			}

		}
		void _namesIni(nilnul.fs.git.svr.client_.vaulted.repo.names_.Dict value)
		{

			this.listBox1.Items.Clear();
			if (value is null)
			{
				return;
			}
			foreach (var item in value)
			{
				var listViewItem = new ListViewItem(
					item.Key
				);
				setTag(ref listViewItem, item.Value);
				//listViewItem.Tag = item.data.client;
				this.listBox1.Items.Add(listViewItem);
			}

		}

		[Obsolete(nameof(names),false)]
		public nilnul.fs.git.svr.client.repo.sortie_.byHost.biject_.Nom1nonemptyEs nameds
		{
			get
			{
				//var schema = fs.git.module.repo._cfg_.remote_._pub.Settings1.Schema;
				//var schemaAsTxts = schema.Cast<string>();

				//var schemaTyped = schema.Cast<string>().Select(
				//	s => nilnul.txt_.vered_._id.Nom.CreateByAppending_ofId(s)
				//).ToArray();

				return new nilnul.fs.git.svr.client.repo.sortie_.byHost.biject_.Nom1nonemptyEs(
					this.listBox1.Items.Cast<ListViewItem>().Select(
						item =>
						(
							(git.svr.client.RepoI)item.Tag
							,

							item.Text.Split('-')

						//schemaAsTxts.Concat(
						//	(IEnumerable<string>)
						//	new[] {
						//		item.Text
						//	}
						//)


						)
					)
				);
			}
			set
			{

				_namedsSet(value);
				report?.Invoke();
			}
		}

		public nilnul.fs.git.svr.client_.vaulted.repo.names_.Dict names
		{
			get
			{
				//var schema = fs.git.module.repo._cfg_.remote_._pub.Settings1.Schema;
				//var schemaAsTxts = schema.Cast<string>();

				//var schemaTyped = schema.Cast<string>().Select(
				//	s => nilnul.txt_.vered_._id.Nom.CreateByAppending_ofId(s)
				//).ToArray();
				var r = new nilnul.fs.git.svr.client_.vaulted.repo.names_.Dict();

				this.listBox1.Items.Cast<ListViewItem>().Select(
										item =>
										(
											item.Text
											,
											(git.svr.client.RepoI)item.Tag


										)
				).Each(
					x => r.Add(x.Item1, x.Item2)
				);
				return r;
			}
			set
			{

				_namesIni(value);
				report?.Invoke();
			}
		}

		void setTag(ref ListViewItem item, fs.git.svr.client.RepoI noCred)
		{
			item.Tag = noCred;
		}

		private void setTag(ref ListViewItem listitem, git.svr.client_.AccVaultedI noCred)
		{
			///convert to svr.client.RepoI
			///

			fs.git.svr.client.RepoI converted = fs.git.svr.client.repo.of_.AccVaultedClient.Singleton.op(noCred);



			setTag(ref listitem, converted);

			//throw new NotImplementedException();
		}

		fs.git.svr.ClientI getTag(ListViewItem item)
		{
			/// to do converted back
			/// 
			return (item.Tag as fs.git.svr.client.RepoI).connection;
		}


		private void Clients_Load(object sender, EventArgs e)
		{
			loadCfg();
		}

		private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
		{
			//this.listBox1.Items[]

		}

		/// <summary>
		/// when user clicks one existant item to open it
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void listBox1_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			//this.listBox1.SelectedItem;
			var index = this.listBox1.SelectedIndices;
			if (index.Count != 1)
			{
				return;
			}

			var indexOnly = index[0];

			var client = getTag(this.listBox1.Items[indexOnly]); //may be null; null means a new box cancelled

			switch (client)
			{
				case null:
					break;
				//case nilnul.fs.git.svr_.amazon.client_.vaulted.Repo4pub amazon0:
				case nilnul.fs.git.svr_.amazon.client_.AccInVault amazon:

					var lowrContent = new svr.client_.amazon.scrollable.ok.Form1() { };


					lowrContent.accVaulted = amazon;

					var dr = lowrContent.ShowDialog();
					if (dr == DialogResult.OK)
					{
						this.listBox1.Items[indexOnly].Tag = lowrContent.accVaulted;
					}

					break;
				case nilnul.fs.git.svr_.github.client_.Vaulted github11:

					var githubDialog = new svr_.github.clients.choose.scrollable.ok.Form1() { };

					githubDialog.accVaulted = github11;

					var dResult = githubDialog.ShowDialog();
					if (dResult == DialogResult.OK)
					{
						this.listBox1.Items[indexOnly].Tag = githubDialog.accVaulted;
					}
					break;


					break;

				case nilnul.fs.git.svr.client_.accVaulted_.Github github:

					var githubDialog1 = new svr.client_.github.scrollable.ok.Form1() { };

					githubDialog1.accVaulted = github;

					var dResult1111 = githubDialog1.ShowDialog();
					if (dResult1111 == DialogResult.OK)
					{
						this.listBox1.Items[indexOnly].Tag = githubDialog1.accVaulted;
					}
					break;

				case nilnul.fs.git.svr.client_.accVaulted_.GitlabI gitlab:

					var gitlabDialog = new svr.client_.gitlab.scrollable.ok.Form1() { };

					gitlabDialog.accVaulted = new git.svr_.gitlab.client_.AccInVault(gitlab.usr); ;

					var dResult1 = gitlabDialog.ShowDialog();
					if (dResult1 == DialogResult.OK)
					{
						this.listBox1.Items[indexOnly].Tag = gitlabDialog.accVaulted;
					}


					break;

				case nilnul.fs.git.svr_.vs.client_.AccInVault vs:

					var vsDialog = new svr.client_.vs.scrollable.ok.Form1() { };


					vsDialog.accVaulted = vs;

					var dResult11 = vsDialog.ShowDialog();
					if (dResult11 == DialogResult.OK)
					{
						this.listBox1.Items[indexOnly].Tag = vsDialog.accVaulted;
					}

					break;

				case nilnul.fs.git.svr_.azure.client_.Vaulted vs11:

					var vsDialog11 = new svr_.azure.client.scrollable.ok.Form1() { };


					vsDialog11.accVaulted = vs11;

					var dResult111 = vsDialog11.ShowDialog();
					if (dResult111 == DialogResult.OK)
					{
						this.listBox1.Items[indexOnly].Tag = vsDialog11.accVaulted;
					}

					break;

				default:
					throw new UnexpectedTypeException(
						client.ToString()
					);
					//throw new NotImplementedException();

			}




		}
		public void add(nilnul.fs.git.svr.client_.accVaulted.Named client)
		{
			var listViewItem = new ListViewItem(client.name);
			listViewItem.Tag = client.client;
			this.listBox1.Items.Add(listViewItem);


			// Place the newly-added item into edit mode immediately
			listViewItem.BeginEdit();
		}
		/// <summary>
		/// create, then add
		/// </summary>
		void createAndAdd()
		{

			var dialog = new _clients.add_.orgable.ok.Form1();

			var r = dialog.ShowDialog();

			if (r == DialogResult.OK)       ///edited and confirmed
			{
				var listitem = new ListViewItem(
					dialog.svr
				);
				setTag(ref listitem, dialog.noCred);

				var added = this.listBox1.Items.Add(listitem);
				added.BeginEdit();

			}


		}


		void addNameFirst()
		{
			///add one with null as the backend unnamed-client
			var listViewItem = new ListViewItem("client1");
			this.listBox1.Items.Add(listViewItem);

			// Place the newly-added item into edit mode immediately
			listViewItem.BeginEdit();

		}

		private void addToolStripMenuItem_Click(object sender, EventArgs e)
		{
			createAndAdd();

		}

		private void removeToolStripMenuItem_Click(object sender, EventArgs e)
		{

			//ListViewHitTestInfo lstHitTestInfo = listBox1.HitTest(e..X, e.Y);
			//                if (e.Button == MouseButtons.Right)
			//                {
			//                    if (lstHitTestInfo.Item != null)
			//                    {
			//                        listView1.ContextMenuStrip = contextMenuStrip1;
			//                    }
			//                }
			if (listBox1.SelectedItems.Count != 0)
			{
				foreach (ListViewItem LItem in listBox1.SelectedItems)
				{
					//Your code

					listBox1.Items.Remove(LItem);


				}
				report?.Invoke();
			}
		}

		private void listBox1_AfterLabelEdit(object sender, LabelEditEventArgs e)
		{
			report?.Invoke();
		}
	}
}
