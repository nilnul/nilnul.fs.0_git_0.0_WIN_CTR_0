﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nilnul.fs._git_._WIN_CTR_.svr_.azure.client
{
	public partial class Scrollable : UserControl
	{
		public Scrollable()
		{
			InitializeComponent();
		}

		public nilnul.fs.git.svr_.azure.client_.Vaulted accVaulted
		{
			get
			{
				return this.svr_client__Vs1.accVaulted;
			}
			set
			{
				this.svr_client__Vs1.accVaulted = value;
			}
		}

		public void saveCred()
		{
			this.svr_client__Vs1.saveCred();
		}
	}
}
