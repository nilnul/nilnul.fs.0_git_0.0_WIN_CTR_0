﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nilnul.fs._git_._WIN_CTR_.svr_.azure.client.scrollable
{
	public partial class svr_client__vs_scrollable_Ok : UserControl
	{
		public svr_client__vs_scrollable_Ok()
		{
			InitializeComponent();
		}

		public event Action oked;
		private void ok1_oked()
		{
			//save it
			this.svr_client__vs_Scrollable1.saveCred();
			oked?.Invoke();

		}

		public nilnul.fs.git.svr_.azure.client_.Vaulted accVaulted
		{
			get
			{
				return this.svr_client__vs_Scrollable1.accVaulted;
			}
			set
			{
				this.svr_client__vs_Scrollable1.accVaulted = value;
			}
		}
	}
}
