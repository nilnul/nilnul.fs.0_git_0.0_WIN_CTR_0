﻿
namespace nilnul.fs._git_._WIN_CTR_.svr_.azure.client.scrollable.ok
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.svr_client__vs_scrollable_Ok1 = new nilnul.fs._git_._WIN_CTR_.svr_.azure.client.scrollable.svr_client__vs_scrollable_Ok();
			this.SuspendLayout();
			// 
			// svr_client__vs_scrollable_Ok1
			// 
			this.svr_client__vs_scrollable_Ok1.accVaulted = ((nilnul.fs.git.svr_.azure.client_.Vaulted)(resources.GetObject("svr_client__vs_scrollable_Ok1.accVaulted")));
			resources.ApplyResources(this.svr_client__vs_scrollable_Ok1, "svr_client__vs_scrollable_Ok1");
			this.svr_client__vs_scrollable_Ok1.Name = "svr_client__vs_scrollable_Ok1";
			this.svr_client__vs_scrollable_Ok1.oked += new System.Action(this.ok1_oked);
			// 
			// Form1
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.svr_client__vs_scrollable_Ok1);
			this.Name = "Form1";
			this.ResumeLayout(false);

		}

		#endregion

		private svr_client__vs_scrollable_Ok svr_client__vs_scrollable_Ok1;
	}
}