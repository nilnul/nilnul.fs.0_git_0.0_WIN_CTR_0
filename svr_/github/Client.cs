﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nilnul.fs._git_._WIN_CTR_.svr_.github
{
	/// <summary>
	/// <see cref="github.client_.svr__github_client__RscAcced"/> would allow us to put a different username in Urn.
	/// </summary>
	public partial class svr__github_Client : UserControl,svr._client_.SaveCredI
	{
		public svr__github_Client()
		{
			InitializeComponent();
		}

		public string prod
		{
			get
			{
				return this.svr__product_header1agent.val;
			}
			set
			{
				this.svr__product_header1agent.val = value;
			}
		}


		public bool beOk {
			get {

				return this.svr__product_header1agent.beOk;

			}
		}



		public nilnul.fs.git.svr_.github.client_.Vaulted accVaulted
		{
			get
			{
				return new git.svr_.github.client_.Vaulted( this.prod, this.cred1.username);
			}
			set
			{
				this.prod = value.prod;
				this.cred1.rsc = value.credKey4push; //expect a trigger to load cred.
				/// todo: username
			}
		}



		private void Svr__header4product_Load(object sender, EventArgs e)
		{
			//this.svr__vs_Prefix1.load();


		}
		private void Cred1_Load()
		{
			/// todo: rsc changing product.
			///
			this.cred1.loadRsc(
				accVaulted.credKey4push
				);

		}
		private void Cred1_Load(object sender, EventArgs e)
		{
			Cred1_Load();

			//if (
			//	nilnul.txt.nulable.be_.NeitherNulNorWhite.Singleton.be(
			//		nilnul.fs.git.Properties.Settings.Default.svr_vs__prefix
			//	)
			//)
			//{
			//	this.cred1.loadRsc(
			//		nilnul.fs.git.svr_.vs_.prefixed.client_._CfgedX.CredRsc
			//	);
			//}

		}



		public void saveCred() {
			

			this.cred1.save();

		}

		private void svr__vs_Prefix1_report()
		{
			if (this.svr__product_header1agent.beOk)
			{
				Cred1_Load();
			}
		}
	}
}
