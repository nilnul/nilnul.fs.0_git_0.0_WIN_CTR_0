﻿
namespace nilnul.fs._git_._WIN_CTR_.svr_.github.client.scrollable
{
	partial class svr_client__github_scrollable_Ok
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.ok1 = new _nilnul_._CTR4WIN_.Ok();
            this.svr_client__amazon_Scrollable1 = new Scrollable();
            ((System.ComponentModel.ISupportInitialize)(this.ok1)).BeginInit();
            this.ok1.Panel1.SuspendLayout();
            this.ok1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ok1
            // 
            this.ok1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ok1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.ok1.Location = new System.Drawing.Point(0, 0);
            this.ok1.Name = "ok1";
            this.ok1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // ok1.Panel1
            // 
            this.ok1.Panel1.Controls.Add(this.svr_client__amazon_Scrollable1);
            this.ok1.Size = new System.Drawing.Size(484, 426);
            this.ok1.SplitterDistance = 376;
            this.ok1.TabIndex = 0;
            this.ok1.oked += new System.Action(this.ok1_oked);
            // 
            // svr_client__amazon_Scrollable1
            // 
            this.svr_client__amazon_Scrollable1.AutoScroll = true;
            this.svr_client__amazon_Scrollable1.AutoScrollMinSize = new System.Drawing.Size(404, 251);
            this.svr_client__amazon_Scrollable1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.svr_client__amazon_Scrollable1.Location = new System.Drawing.Point(0, 0);
            this.svr_client__amazon_Scrollable1.MinimumSize = new System.Drawing.Size(50, 50);
            this.svr_client__amazon_Scrollable1.Name = "svr_client__amazon_Scrollable1";
            this.svr_client__amazon_Scrollable1.Size = new System.Drawing.Size(484, 376);
            this.svr_client__amazon_Scrollable1.TabIndex = 0;
            // 
            // svr_client__amazon_scrollable_Ok
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ok1);
            this.Name = "svr_client__amazon_scrollable_Ok";
            this.Size = new System.Drawing.Size(484, 426);
            this.ok1.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ok1)).EndInit();
            this.ok1.ResumeLayout(false);
            this.ResumeLayout(false);

		}

		#endregion

		private _nilnul_._CTR4WIN_.Ok ok1;
		private svr_.github.client.Scrollable svr_client__amazon_Scrollable1;
	}
}
