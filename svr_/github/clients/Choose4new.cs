﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using nilnul.fs.git.svr.client_.accVaulted_;
using nilnul.fs.git.svr_.amazon.client_;

namespace nilnul.fs._git_._WIN_CTR_.svr_.github.clients
{
	/// <summary>
	/// according to spec, fill the data.
	/// </summary>
	public partial class svr__github_clients_Choose4new :
		UserControl
		,
		svr._client_.SaveCredI
	{
		public const string PERSONAL = "personal";

		public const string ORG = "org";

		public svr__github_clients_Choose4new()
		{
			InitializeComponent();
			this.comboBox1.DataSource = new[] { PERSONAL, ORG };
		}

		void _valide()
		{
		}

		public string personal0org
		{
			get
			{
				return this.comboBox1.Text;
			}
			set
			{
				this.comboBox1.Text = value;
			}
		}


		public string personal = "personal";
		public nilnul.fs.git.svr.client_.AccVaultedI accVaulted
		{
			get
			{
				return accVaultedAsClass;
			}
		}
		public nilnul.fs.git.svr_.github.client_.Vaulted accVaultedAsClass
		{
			get
			{
				switch (this.comboBox1.Text)
				{
					case "personal":
						return /*new git.svr.client_.AccVaulted*/(
							(
							(svr_.github.client_.svr__github_client__RscAcced)this.splitContainer1.Panel2.Controls[0]
							).accVaulted

						);
					case "org":
						return /*new git.svr.client_.AccVaulted*/(
							(
							(svr_.github.client_.orged.Scrollable)this.splitContainer1.Panel2.Controls[0]
							).accVaulted

						);


					default:
						return null;
						throw new UnexpectedReachException();
						break;
				}
			}
			set
			{
				switch (value)
				{
					case nilnul.fs.git.svr_.github.client_.vaulted_.ForOrg githubOrg:
						this.comboBox1.Text = ORG;

						lowerLoad(githubOrg);
						break;

					case nilnul.fs.git.svr_.github.client_.Vaulted github:
						this.comboBox1.Text = PERSONAL;
						lowerLoadNonorg(github);
						break;
					default:
						lowerLoadUnknown();
						//throw new NotImplementedException();
						break;
				}
			}
		}

		svr_.github.client_.svr__github_client__RscAcced _personalCtr;


		private void lowerLoadNonorg(nilnul.fs.git.svr_.github.client_.Vaulted github)
		{
			if (_personalCtr is null)
			{
				_personalCtr = new
					client_.svr__github_client__RscAcced
					//svr__github_Client

					() { Dock = DockStyle.Fill };
				_personalCtr.accVaulted = github;
				this.splitContainer1.Panel2.Controls.Clear();
				this.splitContainer1.Panel2.Controls.Add(
					_personalCtr
				);
			}
			else
			{
				if (_personalCtr.accVaulted != github)
				{
					_personalCtr.accVaulted = github;

					if (!this.splitContainer1.Panel2.Controls.Contains(_personalCtr))
					{
						this.splitContainer1.Panel2.Controls.Clear();
						this.splitContainer1.Panel2.Controls.Add(
							_personalCtr
						);
					}
				}
			}
		}

		svr_.github.client_.orged.Scrollable _orgCtr;
		private void lowerLoad(nilnul.fs.git.svr_.github.client_.vaulted_.ForOrg github)
		{
			if (_orgCtr is null)
			{
				_orgCtr = new svr_.github.client_.orged.Scrollable() { Dock = DockStyle.Fill };
				_orgCtr.accVaulted = github;


				this.splitContainer1.Panel2.Controls.Clear();

				this.splitContainer1.Panel2.Controls.Add(
					_orgCtr
				);



			}
			else
			{
				if (_orgCtr.accVaulted != github)
				{
					_orgCtr.accVaulted = github;

					if (!this.splitContainer1.Panel2.Controls.Contains(_orgCtr))
					{
						this.splitContainer1.Panel2.Controls.Clear();

						this.splitContainer1.Panel2.Controls.Add(
							_orgCtr
						);

					}

				}
			}



		}



		private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
		{
			switch (this.comboBox1.Text)
			{
				case PERSONAL:


					if (!this.splitContainer1.Panel2.Controls.OfType<svr_.github.svr__github_Client>().Any())
					{
						this.splitContainer1.Panel2.Controls.Clear();
						this.splitContainer1.Panel2.Controls.Add(
							this._personalCtr ??= new client_.svr__github_client__RscAcced() { Dock = DockStyle.Fill }
						);
					}
					break;
				case ORG:
					if (
						!this.splitContainer1.Panel2.Controls.OfType<client_.orged.Scrollable>().Any()
					)
					{

						this.splitContainer1.Panel2.Controls.Clear();
						this.splitContainer1.Panel2.Controls.Add(
							 this._orgCtr ??= new client_.orged.Scrollable() { Dock = DockStyle.Fill, accVaulted=null  }
						);
					}
					break;


				default:
					lowerLoadUnknown();

					break;
			}
		}



		private void lowerLoadUnknown()
		{
			var lowrContent = new svr._clients._add.UnknownClient() { Dock = DockStyle.Fill };

			this.splitContainer1.Panel2.Controls.Clear();

			this.splitContainer1.Panel2.Controls.Add(
				lowrContent
			);

		}




		public void saveCred()
		{
			var contained = this.splitContainer1.Panel2.Controls.Cast<Control>().FirstOrDefault();
			if (contained is null)
			{

			}
			else
			{
				if (contained is nilnul.fs._git_._WIN_CTR_.svr._client_.SaveCredI s)
				{
					s.saveCred();
				}
			}

		}




	}
}
