﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nilnul.fs._git_._WIN_CTR_.svr_.github.clients.choose
{
	public partial class svr__github_clients_choose_Scrollable : UserControl
	{
		public svr__github_clients_choose_Scrollable()
		{
			InitializeComponent();
		}

		public
			//nilnul.fs.git.svr_.github.client_.Vaulted

			nilnul.fs.git.svr_.github.client_.Vaulted
			accVaulted
		{
			get
			{
				return this.svr_client__Github1.accVaultedAsClass;
			}
			set
			{
				this.svr_client__Github1.accVaultedAsClass = value;
			}
		}

		public void saveCred()
		{
			this.svr_client__Github1.saveCred();
		}

	}
}
