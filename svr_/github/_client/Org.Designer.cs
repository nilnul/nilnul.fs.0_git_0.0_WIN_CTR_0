﻿namespace nilnul.fs._git_._WIN_CTR_.svr_.github._client
{
	partial class svr__github__client_Org
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.eg__herit__readonly__multiline__xpn__trigValid_WithInput11 = new nilnul._txt_._WIN_CTR_.eg_.herit_.readonly_.multiline_.xpn_.trigValid.eg__herit__readonly__multiline__xpn__trigValid_WithInput1();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.eg__line__Herit1required = new nilnul._txt_._WIN_CTR_.eg_.herit_.readonly_.multiline_.xpn_.trigValid.withInput.eg__herit__readonly__multiline__xpn__trigValid_withInput_Required();
			((System.ComponentModel.ISupportInitialize)(this.eg__herit__readonly__multiline__xpn__trigValid_WithInput11)).BeginInit();
			this.eg__herit__readonly__multiline__xpn__trigValid_WithInput11.Panel1.SuspendLayout();
			this.eg__herit__readonly__multiline__xpn__trigValid_WithInput11.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.SuspendLayout();
			// 
			// eg__herit__readonly__multiline__xpn__trigValid_WithInput11
			// 
			this.eg__herit__readonly__multiline__xpn__trigValid_WithInput11.Dock = System.Windows.Forms.DockStyle.Fill;
			this.eg__herit__readonly__multiline__xpn__trigValid_WithInput11.Location = new System.Drawing.Point(0, 0);
			this.eg__herit__readonly__multiline__xpn__trigValid_WithInput11.Name = "eg__herit__readonly__multiline__xpn__trigValid_WithInput11";
			this.eg__herit__readonly__multiline__xpn__trigValid_WithInput11.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// eg__herit__readonly__multiline__xpn__trigValid_WithInput11.Panel1
			// 
			this.eg__herit__readonly__multiline__xpn__trigValid_WithInput11.Panel1.Controls.Add(this.splitContainer1);
			this.eg__herit__readonly__multiline__xpn__trigValid_WithInput11.Panel2Collapsed = true;
			this.eg__herit__readonly__multiline__xpn__trigValid_WithInput11.Size = new System.Drawing.Size(423, 313);
			this.eg__herit__readonly__multiline__xpn__trigValid_WithInput11.TabIndex = 0;
			this.eg__herit__readonly__multiline__xpn__trigValid_WithInput11.xpn = "";
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.Location = new System.Drawing.Point(0, 0);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.textBox1);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.eg__line__Herit1required);
			this.splitContainer1.Size = new System.Drawing.Size(423, 313);
			this.splitContainer1.SplitterDistance = 54;
			this.splitContainer1.TabIndex = 1;
			// 
			// textBox1
			// 
			this.textBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textBox1.Location = new System.Drawing.Point(0, 0);
			this.textBox1.Multiline = true;
			this.textBox1.Name = "textBox1";
			this.textBox1.ReadOnly = true;
			this.textBox1.Size = new System.Drawing.Size(54, 313);
			this.textBox1.TabIndex = 1;
			this.textBox1.Text = "Org";
			this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// eg__line__Herit1required
			// 
			this.eg__line__Herit1required.Dock = System.Windows.Forms.DockStyle.Fill;
			this.eg__line__Herit1required.Location = new System.Drawing.Point(0, 0);
			this.eg__line__Herit1required.Name = "eg__line__Herit1required";
			this.eg__line__Herit1required.Size = new System.Drawing.Size(365, 313);
			this.eg__line__Herit1required.TabIndex = 0;
			this.eg__line__Herit1required.val = "";
			this.eg__line__Herit1required.changed += new System.Action(this.Eg__line__Herit1required_changed);
			this.eg__line__Herit1required.Load += new System.EventHandler(this.Eg__line__Herit1_Load);
			// 
			// svr__github__client_Org
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.eg__herit__readonly__multiline__xpn__trigValid_WithInput11);
			this.Name = "svr__github__client_Org";
			this.Size = new System.Drawing.Size(423, 313);
			this.eg__herit__readonly__multiline__xpn__trigValid_WithInput11.Panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.eg__herit__readonly__multiline__xpn__trigValid_WithInput11)).EndInit();
			this.eg__herit__readonly__multiline__xpn__trigValid_WithInput11.ResumeLayout(false);
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel1.PerformLayout();
			this.splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private _txt_._WIN_CTR_.eg_.herit_.readonly_.multiline_.xpn_.trigValid.eg__herit__readonly__multiline__xpn__trigValid_WithInput1 eg__herit__readonly__multiline__xpn__trigValid_WithInput11;
		private _txt_._WIN_CTR_.eg_.herit_.readonly_.multiline_.xpn_.trigValid.withInput.eg__herit__readonly__multiline__xpn__trigValid_withInput_Required eg__line__Herit1required;
		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.TextBox textBox1;
	}
}
