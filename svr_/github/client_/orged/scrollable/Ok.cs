﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nilnul.fs._git_._WIN_CTR_.svr_.github.client_.orged.scrollable
{
	public partial class svr_client__github_scrollable_Ok : UserControl
	{
		public svr_client__github_scrollable_Ok()
		{
			InitializeComponent();
		}

		public event Action oked;
		private void ok1_oked()
		{
			//save it
			this.svr_client__amazon_Scrollable1.saveCred();
			oked?.Invoke();

		}

		public nilnul.fs.git.svr_.github.client_.vaulted_.orged.Repo4pub repoClient
		{
			get
			{
				if (accVaulted is null)
				{
					return null;

				}
				return new git.svr_.github.client_.vaulted_.orged.Repo4pub(accVaulted);
			}
			set
			{
				this.accVaulted = value.client;
			}
		}

		public nilnul.fs.git.svr_.github.client_.vaulted_.ForOrg accVaulted
		{
			get
			{
				return this.svr_client__amazon_Scrollable1.accVaulted;
			}
			set
			{
				this.svr_client__amazon_Scrollable1.accVaulted = value;
			}
		}
	}
}
