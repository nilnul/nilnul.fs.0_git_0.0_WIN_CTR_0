﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nilnul.fs._git_._WIN_CTR_.svr_.github.client_
{
	/// <summary>
	/// vs: <see cref="github.svr__github_Client"/>, this puts username into the rsc, allowing:
	///		-) a usr can have multiple entry in the windows Vault.
	///		-) the credential/login sent to the server in git push/etc would contain a username that is not the account in the urn. and the account in urn is not sent to the server, and it can be a fake value, just to make the urn distinct for retrieval of credential.
	/// </summary>
	public partial class svr__github_client__RscAcced : UserControl,svr._client_.SaveCredI
	{
		public svr__github_client__RscAcced()
		{
			InitializeComponent();
			this.cred1.url = web.url_.SchemedNodewise.Parse($@"https://@github.com/");

		}

		public string prod
		{
			get
			{
				return this.svr__product_header1agent.val;
			}
			set
			{
				this.svr__product_header1agent.val = value;
			}
		}


		public bool beOk {
			get {

				return this.svr__product_header1agent.beOk;

			}
		}



		public nilnul.fs.git.svr_.github.client_.Vaulted accVaulted
		{
			get
			{
				return new git.svr_.github.client_.Vaulted( this.prod, this.cred1.username);
			}
			set
			{
				if (value is null)
				{
					this.prod = "git";
					this.cred1.url = web.url_.SchemedNodewise.Parse($@"https://@github.com/");
					return;

				}

				this.prod = value.prod;

				this.cred1.url = web.url_.SchemedNodewise.Parse($@"https://@github.com/");
				this.cred1.acc = value.username;

				//this.cred1.loadRsc();
				 //expect a trigger to load cred.
			}
		}



		private void Svr__header4product_Load(object sender, EventArgs e)
		{
			//this.svr__vs_Prefix1.load();


		}
		private void Cred1_Load()
		{
			/// todo: rsc changing product.
			///
			this.cred1.loadRsc(
				//accVaulted.credKey4push
			);

		}
		private void Cred1_Load(object sender, EventArgs e)
		{
			Cred1_Load();

			//if (
			//	nilnul.txt.nulable.be_.NeitherNulNorWhite.Singleton.be(
			//		nilnul.fs.git.Properties.Settings.Default.svr_vs__prefix
			//	)
			//)
			//{
			//	this.cred1.loadRsc(
			//		nilnul.fs.git.svr_.vs_.prefixed.client_._CfgedX.CredRsc
			//	);
			//}

		}



		public void saveCred() {
			

			this.cred1.save();

		}

		private void svr__vs_Prefix1_report()
		{
			if (this.svr__product_header1agent.beOk)
			{
				this.cred1.app = this.svr__product_header1agent.val; // this doesnot trigger.
				this.cred1.genRsc(); //updates rsc and other inputs in cred, and trigger a report, which trigger this control to update other inputs.
				//this.cred1.loadRsc();
				//Cred1_Load();
			}
		}
	}
}
