﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nilnul.fs._git_._WIN_CTR_.svr_.github.client_
{
	/// <summary>
	/// connecting to an org account at github.com. 
	/// </summary>
	public partial class svr__github_client__Orged : UserControl
		, svr._client_.SaveCredI
	{
		public svr__github_client__Orged()
		{
			InitializeComponent();
		}

		public string org
		{
			get
			{
				return this.svr__github__client_Org1.val;
			}
			set
			{
				this.svr__github__client_Org1.val = value;
			}
		}

		/// <summary>
		/// github requires a prod <see cref="nilnul.fs.git.svr_.github._client_.HeaderI"/> to identify agent/client software for analytic purpose. could be any value.
		/// </summary>
		public string prod
		{
			get
			{
				return this.product.val;
			}
			set
			{
				this.product.val = value;
			}
		}

		public nilnul.web.url_.SchemedNodewise url
		{
			get
			{
				return this.cred1.url;
			}
			set
			{
				this.cred1.url = value;
			}
		}


		public bool prodOrgBeOk
		{
			get
			{

				return this.svr__github__client_Org1.beOk && this.product.beOk;

			}
		}

		public bool beOk
		{
			get
			{

				return prodOrgBeOk;

			}
		}
		public nilnul.fs.git.svr_.github.client_.vaulted_.orged.Repo4pub repoClient
		{
			get
			{
				if (accVaulted is null)
				{
					return null;

				}
				return new git.svr_.github.client_.vaulted_.orged.Repo4pub(accVaulted);
			}
			set
			{
				this.accVaulted= value.client;
			}
		}
		public nilnul.fs.git.svr_.github.client_.vaulted_.ForOrg accVaulted
		{
			get
			{
				return new git.svr_.github.client_.vaulted_.ForOrg(this.prod, this.cred1.username, this.org);
			}
			set
			{
				if (value is null)
				{
					//this.org = "yourOrgName";
					this.prod = "git";
					this.cred1.url = web.url_.SchemedNodewise.Parse($@"https://@github.com/{(this.svr__github__client_Org1.beOk ? this.org : "")}");
					return;
				}

				this.org = value.org;
				this.prod = value.prod;
				this.cred1.url = (value.url);

				this.cred1.acc = value.username;
				this.cred1.loadRsc();

			}
		}



		private void Svr__productHeader_Load(object sender, EventArgs e)
		{

			//this.svr__vs_Prefix1.load();


		}
		private void Cred1_Load()
		{
			//this.cred1.app = "git";
			//this.cred1.url =  web.url_.SchemedNodewise.Parse("https://github.com");
			this.cred1.loadRsc(

			);

		}
		private void Cred1_Load(object sender, EventArgs e)
		{

			Cred1_Load();

			//if (
			//	nilnul.txt.nulable.be_.NeitherNulNorWhite.Singleton.be(
			//		nilnul.fs.git.Properties.Settings.Default.svr_vs__prefix
			//	)
			//)
			//{
			//	this.cred1.loadRsc(
			//		nilnul.fs.git.svr_.vs_.prefixed.client_._CfgedX.CredRsc
			//	);
			//}

		}



		public void saveCred()
		{


			this.cred1.save();

		}

	

		private void svr__github__client_Org1_report()
		{
			if (this.svr__github__client_Org1.beOk)
			{

				var oldUrl = this.cred1.url;
				if (oldUrl is null)
				{
					this.cred1.url = web.url_.SchemedNodewise.Parse(
						@$"https://@github.com/{this.org}"
					);
					//return;
				}
				else
				{
					oldUrl.nodewise.resource0nul = web._url._nodewise.Resource.Parse(
						$"/{this.org}"
					);

				}
				this.cred1.genRsc();
				this.cred1.loadRsc();
			}
		}

		private void product_report()
		{
			if (product.beOk)
			{

				this.cred1.app = product.val;
				this.cred1.genRsc();
				/// not necessary as the above would trigger loadLogin.
				//this.cred1.loadRsc();
			}
			
		}
	}
}