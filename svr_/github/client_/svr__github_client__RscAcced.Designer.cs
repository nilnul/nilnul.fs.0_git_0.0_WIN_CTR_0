﻿using nilnul.fs._git_._WIN_CTR_.svr_.vs;

namespace nilnul.fs._git_._WIN_CTR_.svr_.github.client_
{
	partial class svr__github_client__RscAcced
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cred1 = new nilnul._web_._CTR4WIN_.nilnul0.acc.rsced_.usered.ctr.acc_rsced__usered_ctr_Vaulted();
			this.svr__product_header1agent = new nilnul.fs._git_._WIN_CTR_.svr_.github._client.svr__github__client_Agent();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.SuspendLayout();
			// 
			// cred1
			// 
			this.cred1.acc = "";
			this.cred1.app = null;
			this.cred1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.cred1.Location = new System.Drawing.Point(0, 0);
			this.cred1.Name = "cred1";
			this.cred1.password = "";
			this.cred1.Size = new System.Drawing.Size(548, 220);
			this.cred1.TabIndex = 1;
			this.cred1.url = null;
			this.cred1.username = "";
			this.cred1.Load += new System.EventHandler(this.Cred1_Load);
			// 
			// svr__product_header1agent
			// 
			this.svr__product_header1agent.Dock = System.Windows.Forms.DockStyle.Fill;
			this.svr__product_header1agent.Location = new System.Drawing.Point(0, 0);
			this.svr__product_header1agent.Name = "svr__product_header1agent";
			this.svr__product_header1agent.Size = new System.Drawing.Size(548, 130);
			this.svr__product_header1agent.TabIndex = 0;
			this.svr__product_header1agent.val = "git";
			this.svr__product_header1agent.report += new System.Action(this.svr__vs_Prefix1_report);
			this.svr__product_header1agent.Load += new System.EventHandler(this.Svr__header4product_Load);
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.Location = new System.Drawing.Point(0, 0);
			this.splitContainer1.Name = "splitContainer1";
			this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.svr__product_header1agent);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.cred1);
			this.splitContainer1.Size = new System.Drawing.Size(548, 354);
			this.splitContainer1.SplitterDistance = 130;
			this.splitContainer1.TabIndex = 2;
			// 
			// svr__github_client__RscAcced
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.splitContainer1);
			this.Name = "svr__github_client__RscAcced";
			this.Size = new System.Drawing.Size(548, 354);
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private nilnul.fs._git_._WIN_CTR_.svr_.github._client.svr__github__client_Agent svr__product_header1agent;
		private nilnul._web_._CTR4WIN_.nilnul0.acc.rsced_.usered.ctr.acc_rsced__usered_ctr_Vaulted cred1;
		private System.Windows.Forms.SplitContainer splitContainer1;
	}
}
