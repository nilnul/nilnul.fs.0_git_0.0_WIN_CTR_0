﻿using nilnul.fs._git_._WIN_CTR_.svr_.vs;

namespace nilnul.fs._git_._WIN_CTR_.svr_.github.client_
{
	partial class svr__github_client__Orged
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.product = new nilnul.fs._git_._WIN_CTR_.svr_.github._client.svr__github__client_Agent();
			this.cred1 = new nilnul._web_._CTR4WIN_.nilnul0.acc.rsced_.usered.ctr.acc_rsced__usered_ctr_Vaulted();
			this.svr__github__client_Org1 = new nilnul.fs._git_._WIN_CTR_.svr_.github._client.svr__github__client_Org();
			this.tableLayoutPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 2;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.Controls.Add(this.product, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.cred1, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.svr__github__client_Org1, 1, 0);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 2;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(837, 577);
			this.tableLayoutPanel1.TabIndex = 3;
			// 
			// product
			// 
			this.product.Dock = System.Windows.Forms.DockStyle.Fill;
			this.product.Location = new System.Drawing.Point(3, 3);
			this.product.Name = "product";
			this.product.Size = new System.Drawing.Size(412, 282);
			this.product.TabIndex = 4;
			this.product.val = "";
			this.product.report += new System.Action(this.product_report);
			this.product.Load += new System.EventHandler(this.Svr__productHeader_Load);
			// 
			// cred1
			// 
			this.cred1.acc = "";
			this.cred1.app = "git";
			this.tableLayoutPanel1.SetColumnSpan(this.cred1, 2);
			this.cred1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.cred1.Location = new System.Drawing.Point(3, 291);
			this.cred1.Name = "cred1";
			this.cred1.password = "";
			this.cred1.Size = new System.Drawing.Size(831, 283);
			this.cred1.TabIndex = 3;
			this.cred1.url = null;
			this.cred1.username = "";
			this.cred1.Load += new System.EventHandler(this.Cred1_Load);
			// 
			// svr__github__client_Org1
			// 
			this.svr__github__client_Org1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.svr__github__client_Org1.Location = new System.Drawing.Point(421, 3);
			this.svr__github__client_Org1.Name = "svr__github__client_Org1";
			this.svr__github__client_Org1.Size = new System.Drawing.Size(413, 282);
			this.svr__github__client_Org1.TabIndex = 5;
			this.svr__github__client_Org1.val = "";
			this.svr__github__client_Org1.report += new System.Action(this.svr__github__client_Org1_report);
			// 
			// svr__github_client__Orged
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.tableLayoutPanel1);
			this.Name = "svr__github_client__Orged";
			this.Size = new System.Drawing.Size(837, 577);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;

		private nilnul.fs._git_._WIN_CTR_.svr_.github._client.svr__github__client_Agent product;

		private 
			nilnul._web_._CTR4WIN_.nilnul0.acc.rsced_.usered.ctr.acc_rsced__usered_ctr_Vaulted

			cred1;

		private _client.svr__github__client_Org svr__github__client_Org1;
		
	}
}
